#!/bin/sh

if [ -d "/home/" ]; then
	cd /home/
else
	cd /usr/
fi

showmsg () {
	NO3_var=$3
	NO4_var=$4

	if [ ! "$NO3_var" ]; then
		NO3_var=$color_yellow
	fi

	if [ ! "$NO4_var" ]; then
		NO4_var=$color_white
	fi

	echo
	echo -e $NO3_var$1$NO4_var
	if [ "$2" ]; then
		echo -e $NO3_var$2$NO4_var
	fi
	echo -e
}

color_cyan="  [40;36m"
color_red="  [40;31m"
color_yellow="  [40;33m"
color_green="  [40;32m"
color_white="  [40;37m"

if [ `uname` = 'FreeBSD' ]; then
	file_get='fetch'
else
	file_get='wget'
fi

echo $color_green
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "+"
echo "+ [2] USA download mirror"
echo "+ [1] China telecom download mirror"
echo "+ [0] Cancel."
echo "+"
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
showmsg "请选择一个下载点，然后回车！"   "Please select a download mirror and press enter! "
read mirror
echo $color_white

if [ "$mirror" = '1' ]; then
	down="down"
	#echo "nameserver 202.96.199.133" > /etc/resolv.conf
	#echo "nameserver 202.106.0.20" >> /etc/resolv.conf
	#echo "nameserver 202.96.0.133" >> /etc/resolv.conf
elif [ "$mirror" = '2' ]; then
	down="down3"
	#echo "nameserver 208.67.222.222" > /etc/resolv.conf
	#echo "nameserver 8.8.8.8" >> /etc/resolv.conf
	#echo "nameserver 202.96.0.133" >> /etc/resolv.conf
elif [ "$mirror" = '3' ]; then
	down="down3"
	#echo "nameserver 208.67.222.222" > /etc/resolv.conf
	#echo "nameserver 8.8.8.8" >> /etc/resolv.conf
	#echo "nameserver 202.96.0.133" >> /etc/resolv.conf
elif [ "$mirror" = '0' ]; then
	exit;
elif [ "$mirror" = '' ]; then
	down="down"
fi

rm -rf ./zijidelu_install ./zijidelu_install.tar.gz

if [ ! -f "./zijidelu_install.tar.gz" ] && [ ! -d "./zijidelu_install" ] && [ ! -d "../zijidelu_install" ]; then
	showmsg    "开始下载zijidelu_install.tar.gz"    "Starting download zijidelu_install.tar.gz"
	sleep 2
	`${file_get} http://down.zijidelu.org/zijidelu_install.tar.gz`

	if [ ! -e "./zijidelu_install.tar.gz" ]; then
		showmsg    "zijidelu_install.tar.gz下载失败"    "Can't to download zijidelu_install.tar.gz"
		exit
	fi
fi

if [ "$2" = '-y' ] || [ "$2" = '-Y' ]; then
	install_extends='allow_pdo, allow_mysqli';
else
	install_extends='';
fi
echo "$install_extends" > /tmp/install_extends.txt;

if [ -f zijidelu_install.tar.gz ]; then
	tar -zxvf zijidelu_install.tar.gz
	chmod -R 700 ./zijidelu_install
fi

if [ -d "zijidelu_install" ]; then
	cd ./zijidelu_install
fi

./update.sh "$1" ${down}

