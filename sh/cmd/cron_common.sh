#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

. "/usr/local/LuNamp/cmd/common.inc"

if [ "`date|grep ' 23'`" ]; then
	ntpdate time.windows.com
fi

if [ `ps -Af | grep apache_LuManager/bin | grep -vc 'grep'` = '0' ]; then
	$cmd_root/apache_LuManager-reload
fi
sleep 2;

tmp_path='/usr/local/LuManager/Runtime/Temp'
cd $tmp_path
if [ "$1" = '' ]; then
	if [ -f "$tmp_path/lum_cron.lock" ]; then
		echo "$tmp_path/lum_cron.lock was exist!"
		exit;
	else
		echo "#" > "$tmp_path/lum_cron.lock";
	fi
fi

lum_port='8888'; #如果在LuNamp配置文件如果定义了lum_port，此值将失效
LuNamp_config_file='/home/lum_safe_files/config.sh';#LuNamp配置文件
if [ -f "$LuNamp_config_file" ]; then
	. "$LuNamp_config_file";
fi

/usr/local/curl/bin/curl --location -D lum_cookie_local.txt --connect-timeout 3600 "http://127.0.0.1:${lum_port}/index.php?m=Public&a=cron&$1"
if [ "$1" = '' ]; then
	rm -f $tmp_path/lum_cron.lock;
fi

#if [ `ps -Af | grep apache_LuManager/bin | grep -vc 'grep'` = '0' ]; then
#	$cmd_root/apache_LuManager-reload
#fi
