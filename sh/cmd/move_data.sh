#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

. "/usr/local/LuNamp/cmd/common.inc"

if [ `uname` != 'Linux' ]; then
	showmsg "非Linux不可运行此脚本"    "Can't run this script."
	exit;
fi

source_dir="$1";#源目录(/home)
target_dir="$2";#目标目录(/data)
remount="$3";#是否重新挂载(true)

if [ "${source_dir}" = '' ]; then
	showmsg "请输入源路径"    "The source dir name is empty."
	exit;
fi

if [ "${target_dir}" = '' ]; then
	showmsg "请输入目标路径"    "The target dir name is empty."
	exit;
fi

#在闭LU的所有组件
if [ "`pgrep 'mysqld'`" ]; then
	lu-stop
fi

#复制文件至新目录下
cp -a ${source_dir}/* ${target_dir}/

if [ "${remount}" = 'true' ]; then
	#修改/etc/fstab文件
	sed -i "s@$target_dir@$source_dir@g" /etc/fstab

	#umount
	umount ${target_dir}

	#重命名
	mv ${source_dir} "${source_dir}.old"
	
	#重新挂载
	mount -a

	df_h=`df -h`

	if [ `echo $df_h|grep -c ${source_dir}` \> 0 ]; then
		lu-repair
		lu-start
		showmsg "${color_green}数据转移成功"    "The data was moved successfully.${color_white}"
	else
		showmsg "${color_red}数据转移失败"    "The data was moved failed.${color_white}"
	fi
else
	lu-repair
	lu-start
	showmsg "${color_green}数据转移成功"    "The data was moved successfully.${color_white}"
fi


