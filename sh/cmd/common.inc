#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

LuNamp_root='/usr/local/LuNamp'
LuManager_root='/usr/local/LuManager'

mysql_data_path="/home/mysql_data";#mysql数据库路径
pgsql_data_path="/home/pgsql_data";#pgsql数据库路径
if [ -f "/home/lum_safe_files/cmd/config.inc.sh" ]; then
	. "/home/lum_safe_files/cmd/config.inc.sh";
fi

export PATH="/usr/local/LuNamp/cmd/:/usr/local/LuNamp/other_cmd/:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin"
export LD_LIBRARY_PATH="/usr/local/mysql/lib:/usr/local/mysql/lib/mysql:/usr/local/pgsql/lib:/usr/lib:/lib:/usr/local/lib"

color_cyan="  [40;36m"
color_red="  [40;31m"
color_yellow="  [40;33m"
color_green="  [40;32m"
color_white="  [40;37m"

cmd_root="/usr/local/LuNamp/cmd"

apache_root="/usr/local/apache"
apache_LuManager_root="/usr/local/apache_LuManager"
mysql_root="/usr/local/mysql"
pgsql_root="/usr/local/pgsql"
nginx_root="/usr/local/nginx"
pureftpd_root="/usr/local/pureftpd"

has_vhost='0';
if [ -f '/usr/local/nginx/conf/vhost.conf' ] && [ "`cat /usr/local/nginx/conf/vhost.conf|grep -c '\(include\|/home/ftp/LuNamp\)'`" != '0' ]; then
	has_vhost=1;
fi

if [ `uname` = 'FreeBSD' ]; then
	my_wget='fetch';
	kill_all='killall';
else
	my_wget='wget';

	linux_version_1=`head -1 /etc/issue`;
	linux_version_2=`echo $linux_version_1|cut -c1-10`;

	if [ `echo $linux_version_2 | egrep -c 'CentOS'` = "1" ]; then
		linux_version='CentOS';
	elif [ `echo $linux_version_2 | egrep -c 'Red Hat'` = "1" -o `echo $linux_version_2 | egrep -c 'RedHat'` = "1" ]; then
                linux_version='RedHat';
	elif [ `echo $linux_version_2 | egrep -c 'Debian'` = "1" ]; then
		linux_version='Debian';
	elif [ `echo $linux_version_2 | egrep -c 'Ubuntu'` = "1" ]; then
		linux_version='Ubuntu';
        fi
	if [ -f '/usr/bin/killall' ] || [ -f '/usr/sbin/killall' ]; then
		kill_all='killall';
	else
		kill_all='skill -KILL';
		#kill_all='pkill';
	fi
fi

kill_soft () {
	if [ "`echo $1|egrep '^[0-9]+$'`" ]; then
		port=`netstat -anp|grep LISTEN|grep ":$1 "|awk '{print $7}'|awk -F '/' '{print $1}'`
		if [ "$port" = '' ]; then
			echo '0';
			exit;
		fi
		kill -9 ${port};
	fi
	progress_id=`pgrep "$1"`
	if [ "$progress_id" != '' ]; then
		for id in ${progress_id}; do
			kill -9 ${id};
		done
	fi
}

# 1. s/r
# 2. file
str_replace () {
	if [ `uname` = 'Linux' ]; then
		sed -i "s/$1/g" $2
	else
		sed "s/$1/g" $2 > ${2}.bak
		sed "s/------------search--------------/------------replace--------------/g" ${2}.bak > ${2}
		rm -f ${2}.bak
	fi
}

showmsg() {
	echo $1;
	echo $2;
}
