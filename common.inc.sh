#!/bin/sh
#
# ******************************************************************************
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# 作者：		刘新（网名：爱洞特漏）
# 电话：		+86-13688995218
# ------------------------------------++++
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# Author:    Liu Xin
# Tel:       +86-13688995218
# ------------------------------------++++
# Thank you for choosing zijidelu's software!
# ******************************************************************************
#

LuNamp_root='/usr/local/LuNamp'
LuManager_root='/usr/local/LuManager'

color_cyan="[40;36m"
color_red="[40;31m"
color_yellow="[40;33m"
color_green="[40;32m"
color_white="[40;37m"

this_dir=`pwd`

system_hardware=`uname -m`;
uname=`uname`
server_name=`uname -n`;

if [ ! -d '/home' ]; then
	mkdir /usr/home
	ln -s /usr/home /home
fi

if [ ! -d "/home/hosts_backup" ]; then
	mkdir -p /home/hosts_backup
fi

if [ ! -d "/home/mysqls_backup" ]; then
	mkdir -p /home/mysqls_backup
fi

if [ ! -d "/home/lum_safe_files" ]; then
	mkdir -p /home/lum_safe_files
fi

if [ `uname` = 'FreeBSD' ]; then
	root_group='wheel'
	file_get='fetch'

	user_del='pw userdel';
	user_add='pw useradd';
	group_del='pw groupdel';
	group_add='pw groupadd';

	kill_all='killall';

	if [ "$system_hardware" = 'amd64' ]; then
		system_bit='64'
	else
		system_bit='32'
	fi
else
	root_group='root'
	file_get='wget '

	if [ -d "/etc/sysconfig" ] && [ "`yum list installed |grep bash`" ]; then
		linux_type='RedHat';
	else
		linux_type='Debian';
	fi

	if [ "$system_hardware" = 'x86_64' ]; then
		system_bit='64'
	else
		system_bit='32'
	fi

	user_del='userdel';
	user_add='useradd';
	group_del='groupdel';
	group_add='groupadd';

	kill_all='skill -KILL';
fi

if [ "$system_bit" = '32' ]; then
	system_jg='i386'
else
	system_jg='x86_64'
fi

showmsg () {
	NO3_var=$3
	NO4_var=$4

	if [ ! "$NO3_var" ]; then
		NO3_var=$color_yellow
	fi

	if [ ! "$NO4_var" ]; then
		NO4_var=$color_white
	fi

	echo
	echo $NO3_var$1$NO4_var
	if [ "$2" ]; then
		echo $NO3_var$2$NO4_var
	fi
	echo
}

cc () {
	chown -R $2 $3
	chmod -R $1 $3
}

repair_files_permission() {
	if [ -f "${LuManager_root}/Common/F3.sh" ] && [ ! -f "${LuNamp_root}/cmd/F3.sh" ]; then
		cp -a ${LuManager_root}/Common/*.sh ${LuNamp_root}/cmd/
	fi

	if [ "$1" ]; then
		#1.0.15以下版本
		cc    '0700'    "LuManager:LuManager"    "$LuManager_root"
	else
		cc    '0500'    "LuManager:LuManager"    "$LuManager_root"
		cc    '0700'    "LuManager:LuManager"    "${LuManager_root}/Conf"
		cc    '0700'    "LuManager:LuManager"    "${LuManager_root}/Runtime"
		cc    '0700'    "LuManager:LuManager"    "/home/lum_safe_files"
	fi

	cc    '571'     "root:LuManager"    "$LuNamp_root"
	
	cc    '0770'    "lu_mysql:lu_mysql"    "/home/mysql_data"
	cc    '0770'    "postgres:postgres"    "/home/pgsql_data"

	cc    '4550'    "root:LuManager"    "$LuNamp_root/cmd/L3.sh"
	cc    '4550'    "root:LuManager"    "$LuNamp_root/cmd/L6.sh"
	cc    '4550'    "root:LuManager"    "$LuNamp_root/cmd/F3.sh"
	cc    '4550'    "root:LuManager"    "$LuNamp_root/cmd/F6.sh"

	if [ -f "/usr/local/apache/logs/access_log" ]; then
		echo "" > /usr/local/apache/logs/access_log;
		echo "" > /usr/local/apache/logs/error_log;
	fi

	if [ -f "/usr/local/apache_LuManager/logs/access_log" ]; then
		echo "" > /usr/local/apache_LuManager/logs/access_log;
		echo "" > /usr/local/apache_LuManager/logs/error_log;
	fi

	if [ -f "/usr/local/nginx/logs/access.log" ]; then
		echo "" > /usr/local/nginx/logs/access.log;
		echo "" > /usr/local/nginx/logs/error.log;
	fi
}

if [ `id -u` != "0" ]; then
	showmsg    "请用root用户安装LuManager（请在命令行输入：su root）！"    "Please use root user to install LuManager (cmd: su root)!"
	exit;
fi

# 1. s
# 2. r
# 3. file
# 4. remove backup file
str_replace () {
	sed "s/$1/$2/g" $3 > ${3}.bak
	sed "s/------------search--------------/------------replace--------------/g" ${3}.bak > ${3}
	if [ "$4" ]; then
		rm -rf ${3}.bak
	fi
}


install_LuNamp() {
	if [ ! -f "/var/tmp/LuNamp_install_from_script.lock" ]; then
		echo "#" > "/var/tmp/LuNamp_install_from_script.lock";
	fi

	if [ "`echo $2|grep -c '^1\.'`" -eq "1" ]; then
		LuNamp_ver='_last';#可以换成2.3
	else
		LuNamp_ver='_last';
	fi

	#如果dev文件存在，则下载测试包
	if [ -f 'dev' ]; then
		LuNamp_ver="${LuNamp_ver}/dev"
	fi

	if [ -f "/tmp/update_LuNamp.lock" ] || [ ! -d "${LuNamp_root}/cmd" ] || [ ! -d "${LuNamp_root}/pm" ]; then
		allow_rpm=1
		#1. 当没有安装过LuNamp时
		#2. $4不等于source时
		#3. 不为redhat系列系统
		#4. 非稳定版本，不能用rpm
		#[ -f '/var/tmp/lum_ver.txt' ] && [ "`cat '/var/tmp/lum_ver.txt'|grep -c '[a-z]'`" -eq '0' ] &&     alpha和beta版本不允许使用rpm安装
		
		#-----------------------------源码安装
		file_du='';

		if [ -d "./LuNamp" ]; then
			cd LuNamp
			#if [ ! -d "${LuNamp_root}/cmd" ]; then
			#	mkdir -p ${LuNamp_root}/cmd
			#fi
			#cp -R ./code/cmd/* ${LuNamp_root}/cmd/
			./i.sh $3
		else
			showmsg    "LuNamp不存在，安装失败"    "LuNamp is not exists"
		fi

	fi

	echo

	if [ ! -d "${LuNamp_root}/cmd" ] || [ ! -d "${LuNamp_root}/pm" ]; then
		showmsg    "LuNamp安装失败"    "LuNamp was install failed"
		exit;
	fi

	cd $this_dir
	#rm -rf ${LuNamp_root}/cmd
	#cp -a ./sh/cmd ${LuNamp_root}/
	cp -a ./sh/F* ${LuNamp_root}/cmd/
	cp -a ./sh/L* ${LuNamp_root}/cmd/
	chmod -R 700 ${LuNamp_root}/cmd/*
	${LuNamp_root}/cmd/lu-repair

	rm -rf /tmp/LuNamp_install_status.txt
}

install_LuManager () {
	if [ -e '/tmp/LuNamp_install_status.txt' ] && [ ! -e "$LuManager_root/index.php" ]; then
		LuNamp_install_status_contents=`cat '/tmp/LuNamp_install_status.txt'`
		showmsg    "LuNamp安装失败（${LuNamp_install_status_contents}）"    "The LuNamp is not installed!(${LuNamp_install_status_contents})"
		exit;
	fi

	cd $this_dir
	
	file_du='';
	if [ -f LuManager_last.tar.gz ]; then
		file_du=`du -s LuManager_last.tar.gz`
	fi
	#if [ ! "`echo $file_du|grep '[0-9]\{4\}'`" ] && [ ! -d "./LuManager" ]; then
	if [ ! "`echo $file_du|grep '[0-9]\{4\}'`" ]; then
		showmsg    "开始下载LuManager"    "Starting download LuManager"
		sleep 2
		if [ "$2" ]; then
			$file_get http://$1.zijidelu.org/lum$2/LuManager_last.tar.gz
		else
			$file_get http://$1.zijidelu.org/LuManager_last.tar.gz
		fi
		
		#如果指定了版分号却没文件
		file_du='';
		if [ -f LuManager_last.tar.gz ]; then
			file_du=`du -s LuManager_last.tar.gz`
		fi
		if [ ! "`echo $file_du|grep '[0-9]\{4\}'`" ]; then
		#if [ ! -e "./LuManager_last.tar.gz" ]; then
			$file_get http://$1.zijidelu.org/LuManager_last.tar.gz
		fi
		
		file_du='';
		if [ -f LuManager_last.tar.gz ]; then
			file_du=`du -s LuManager_last.tar.gz`
		fi
		if [ ! "`echo $file_du|grep '[0-9]\{4\}'`" ]; then
			showmsg    "LuManager下载失败或不完整"    "Can't to download LuManager"
			exit
		fi
	fi

	pm_exists=0
	if [ -e "$LuManager_root/pm" ]; then
		pm_exists=1;
	fi
	pp_exists=0
	if [ -e "$LuManager_root/pp" ]; then
		pp_exists=1;
	fi

	file_du='';
	if [ -f LuManager_last.tar.gz ]; then
		file_du=`du -s LuManager_last.tar.gz`
	fi
	if [ "`echo $file_du|grep '[0-9]\{4\}'`" ]; then
		rm LuManager
		tar -zxvf LuManager_last.tar.gz
	fi

	mkdir -p $LuManager_root
	if [ -d "./LuManager/" ]; then
		if [ -f "$LuManager_root/Conf/config.php" ]; then
			backup_file=/home/hosts_backup/LuManager_`date +"%Y%m%d%s"`
			mkdir -p $backup_file
			cp -R $LuManager_root/* $backup_file/
			cat $LuManager_root/Conf/config.php > /home/lum_safe_files/config.php.last
			cc    '0500'    "LuManager:LuManager"    "/home/lum_safe_files/config.php.last"
			
			#ln -sf $LuManager_root/Conf/config.php /root/lum_config.php
			#cc    '0500'    "LuManager:LuManager"    "/root/lum_config.php"
		fi

		rm -rf $LuManager_root
		cp -R ./LuManager    /usr/local/
		
		if [ $pm_exists = 1 ]; then
			ln -s $LuNamp_root/pm $LuManager_root/pm
		fi
		if [ $pp_exists = 1 ]; then
			ln -s $LuNamp_root/pp $LuManager_root/pp
		fi

		if [ -f "$LuManager_root/Common/L3.sh" ]; then
			cp -R $LuManager_root/Common/*.sh $LuNamp_root/cmd/
			#rm -rf $LuManager_root/Common/*.sh
		fi
	else
		showmsg 'LuManager目录不存在，LuManager安装失败！';
		exit;
	fi

	cd $this_dir
	rm -rf ${LuNamp_root}/cmd
	cp -a ./sh/cmd ${LuNamp_root}/
	cp -a ./sh/F* ${LuNamp_root}/cmd/
	cp -a ./sh/L* ${LuNamp_root}/cmd/
	chmod -R 700 ${LuNamp_root}/cmd/*
	${LuNamp_root}/cmd/lu-repair
}

ai_ntpdate () {
	if [ -f '/usr/bin/ntpdate' ] || [ -f '/usr/sbin/ntpdate' ]; then
		ntpdate time.zijidelu.org
	fi
	datetime=`date +"%Y%m%d"`
	release_date=$(($release_date-1));#wrong date time, But can be used.
	if test $datetime -lt $release_date; then
		#showmsg "您的系统时钟错误（${datetime}），请用date命令调整时间"    "Your system clock is wrong(${datetime}), Please use date command change it!"
		#showmsg "FreeBSD like this: date 201012050000(YmdHi), Linux like this: date 12050000(mdHiY)"
		#exit;

		if [ `uname` = 'FreeBSD' ]; then
			date ${release_y}${release_md}2359
		else
			date ${release_md}2359${release_y}
		fi
	fi
}

#-----------------------------------------------------------------------------------------mysql密码
check_mysql_password() {
	#杀掉已有进程
	if [ ! `ps -Af | grep -c 'lu_mysql'` -gt 1 ]; then
		$LuNamp_root/cmd/kill-port "3306" 2>&1 > /dev/null
	fi

	if [ ! `ps -Af | grep -c 'mysqld'` -gt 1 ] && [ -f "$LuNamp_root/cmd/lu-start" ]; then
		$LuNamp_root/cmd/lu-start
	fi

	if [ ! "`pgrep 'mysqld'`" ]; then
		$LuNamp_root/cmd/mysql-start
	fi

	if [ ! `ps -Af | grep -c 'mysqld'` -gt 1 ]; then
		showmsg    "MySQL数据库未启动，请先启动数据库，然后再试!"    "The MySQL was not started, Please start your MySQL and try again!"
		exit;
	fi

	TF="`/usr/local/mysql/bin/mysqladmin -uroot -pzijidelu extended-status|grep '+----'`"
	if [ ! "$TF" ]; then
		showmsg    "请输入您的MySQL的root用户密码!"    "Please enter the mysql root user password!"
		while [ 1 ]; do
			read mysql_password
			TF=`/usr/local/mysql/bin/mysqladmin -uroot -p${mysql_password} extended-status|grep '+----'`
			if [ "$TF" ]; then
				break
			else
				showmsg    "密码错误，请重试!"    "The password is wrong, Please retry!"
				continue;
			fi
		done
	else
		mysql_password='zijidelu'
	fi
	
	if [ ! -f "/home/lum_safe_files/config.php" ] && [ "$mysql_password" != 'zijidelu' ] && [ "$mysql_password" != '' ]; then
		echo '<?php' > "/home/lum_safe_files/config.php";
		echo 'return array(' >> "/home/lum_safe_files/config.php";
			echo "	'DB_TYPE' => 'mysql', // 数据库类型，可选mysql和pgsql(推荐使用pgsql)" >> "/home/lum_safe_files/config.php";
			echo "	'PGSQL_DB_PWD' => 'zijidelu', // 密码（默认值：zijidelu）" >> "/home/lum_safe_files/config.php";
			echo "	'MYSQL_DB_PWD' => '${mysql_password}', // 密码（默认值：zijidelu）" >> "/home/lum_safe_files/config.php";
			#echo "	'LUM_PWD' => '', // LUM的系统管理员的登陆密码和保护密码（忘记登陆密码时使用）" >> "/home/lum_safe_files/config.php";
			echo "	########" >> "/home/lum_safe_files/config.php";
		echo ");" >> "/home/lum_safe_files/config.php";
	fi
}

import_data() {
	if [ -d '/usr/local/mysql/var/mysql' ] && [ ! -d '/home/mysql_data/mysql' ]; then
		./sh/copy_mysql_data.sh
	fi

	#备份数据库
	if [ -f '/home/mysql_data/LuManager/lu_settings.frm' ]; then
		showmsg    "LuManager的数据库已经存在，您要删除旧数据？[y/N]"    "The LuManager data(MySQL) is exists, Do you want to delete it?[y/N]"
		read delete_LuManager_data
		if [ "$delete_LuManager_data" = 'y' ] || [ "$delete_LuManager_data" = 'Y' ]; then
			mkdir -p /home/mysqls_backup
			backup_file=/home/mysqls_backup/LuManager_`date +"%Y%m%d%s"`
			mkdir -p $backup_file
			cp -R /home/mysql_data/LuManager/* $backup_file/
			
			rm -rf /home/mysql_data/LuManager/*
			showmsg    "旧的LuManager数据库备份文件：$backup_file"    "The old LuManager database backup file: $backup_file"
			sleep 1
		fi
	fi

	/usr/local/mysql/bin/mysqladmin -uroot -p${mysql_password} refresh

	if [ ! -f '/home/mysql_data/LuManager/lu_settings.frm' ]; then
		#导入刚下载的LuManager中的sql数据
		/usr/local/mysql/bin/mysql -uroot -p${mysql_password} --default-character-set=utf8 LuManager < LuManager/LuManager.sql
		sleep 5
	fi

	if [ ! -f '/home/mysql_data/LuManager/lu_settings.frm' ]; then
		showmsg 'LuManager数据导入不成功！'    'LuManager data was not imported！'    $color_red    $color_white
		exit
	fi
}

user_and_group_add() {
	$group_add LuManager -g 1500
	$user_add LuManager -u 1500 -g LuManager -s /sbin/nologin -d /dev/null

	$group_add lu_mysql -g 1504
	$user_add lu_mysql -u 1504 -g lu_mysql -s /sbin/nologin -d /dev/null

	$group_add lu_bind -g 1505
	$user_add lu_bind -u 1505 -g lu_bind -s /sbin/nologin -d /dev/null

	$group_add postgres -g 1506
	$user_add postgres -u 1506 -g postgres -d /dev/null

	$group_add lu_ftp -g 1507
	$user_add lu_ftp -u 1507 -g lu_ftp -s /sbin/nologin -d /dev/null

	$group_add zijideluGroup -g 1520
	$user_add zijidelu -u 1520 -g zijideluGroup -s /sbin/nologin -d /dev/null
	return;
}
