#!/bin/sh
#
# ******************************************************************************
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# 作者：		刘新（网名：爱洞特漏）
# 电话：		+86-13688995218
# ------------------------------------++++
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# Author:    Liu Xin
# Tel:       +86-13688995218
# ------------------------------------++++
# Thank you for choosing zijidelu's software!
# ******************************************************************************
#

. './common.inc.sh'

$group_add postgres -g 1506
$user_add postgres -u 1506 -g postgres -d /dev/null












#备份数据库
backup_file=/home/mysqls_backup/LuManager_`date +"%Y%m%d%s"`
mkdir -p $backup_file
cp -R /home/mysql_data/LuManager/* $backup_file/

#删除旧软件（需要升级）
if [ ! -d '/usr/local/pgsql' ]; then
	#rm -rf /usr/local/php*;
	rm -rf /usr/local/nginx
	rm -rf /usr/local/pureftpd
	rm -rf /usr/local/LuNamp/pm
fi

echo "#" > /tmp/update_LuNamp.lock
install_LuNamp $1 "2.1.2" $2 'source'
rm -rf /tmp/update_LuNamp.lock

install_LuManager $1 "2.1.2"

check_mysql_password
/usr/local/mysql/bin/mysql -uroot -p${mysql_password} LuManager < gogogo/2.0/1.1.10_2.0.X.sql
sleep 3

repair_files_permission

if [ -e "/usr/local/LuManager" ]; then
	/usr/local/LuNamp/cmd/lum-restart
	showmsg "LuManager升级完成，谢谢您的使用!"    "LuManager was successfully updated, Thank you for choose it!"
fi

