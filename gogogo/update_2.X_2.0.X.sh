#!/bin/sh
#
# ******************************************************************************
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# 作者：		刘新（网名：爱洞特漏）
# 电话：		+86-13688995218
# ------------------------------------++++
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# Author:    Liu Xin
# Tel:       +86-13688995218
# ------------------------------------++++
# Thank you for choosing zijidelu's software!
# ******************************************************************************
#

. './common.inc.sh'

if [ -f /usr/local/php/bin/php ]; then
	php_ver="`/usr/local/php/bin/php -v`";
else
	php_ver="";
fi
if [ ! "${php_ver}" ] || [ ! -d /usr/local/php ] || [ "`echo $php_ver|grep -c 'PHP 5.3.9'`" = '1' ] || [ "`echo $php_ver|grep -c 'PHP 5.3.8'`" = '1' ]; then
	rm -rf /usr/local/LuNamp /usr/local/php /usr/local/apache /usr/local/pureftpd
else
	if [ ! -f "$LuNamp_root/cmd/" ]; then
		mkdir -p $LuNamp_root/cmd/
	fi
	rm -rf $LuNamp_root/cmd/*
	cp -R ./sh/cmd/* $LuNamp_root/cmd/
	cp -R ./sh/L* ./sh/F* $LuNamp_root/cmd/
fi

if [ ! -d "$LuNamp_root/pm" ]; then
	echo "#" > /tmp/update_LuNamp.lock
	install_LuNamp $1 "2.1.2" $2 'source'
	rm -rf /tmp/update_LuNamp.lock
fi

#备份数据库
backup_file=/home/mysqls_backup/LuManager_`date +"%Y%m%d%s"`
mkdir -p $backup_file
cp -R /home/mysql_data/LuManager/* $backup_file/

if [ ! -f '/home/lum_safe_files/config.php' ]; then
	check_mysql_password
else
	mysql_password='zijidelu'
fi

install_LuManager $1 "2.1.2"
sleep 3

repair_files_permission

if [ -e "/usr/local/LuManager" ]; then
	/usr/local/LuNamp/cmd/lu-restart
	showmsg "LuManager升级完成，谢谢您的使用!"    "LuManager was successfully updated, Thank you for choose it!"
fi

