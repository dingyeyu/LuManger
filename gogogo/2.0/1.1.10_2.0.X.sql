#-------------lu_solutions
	#PHP超时时间
ALTER TABLE  `lu_solutions` ADD  `host_php_timeout` VARCHAR( 10 ) NOT NULL AFTER  `host_cpulimit`;
ALTER TABLE `lu_solutions` ADD `can_use_groups` TEXT NOT NULL AFTER `inheritance` ;
ALTER TABLE `lu_solutions` ADD `cdn_nodes` TEXT NOT NULL AFTER `can_use_groups`;

ALTER TABLE `lu_solutions` ADD `solution_mark` VARCHAR( 15 ) NOT NULL AFTER `cdn_nodes` ,
ADD INDEX ( `solution_mark` ) ;

ALTER TABLE `lu_solutions` ADD `has_ftps` VARCHAR( 3 ) NOT NULL DEFAULT 'off' AFTER `sort` ,
ADD `has_hosts` VARCHAR( 3 ) NOT NULL DEFAULT 'off' AFTER `has_ftps` ,
ADD `has_mysqls` VARCHAR( 3 ) NOT NULL DEFAULT 'off' AFTER `has_hosts` ,
ADD `has_zones` VARCHAR( 3 ) NOT NULL DEFAULT 'off' AFTER `has_mysqls` ,
ADD `has_cdns` VARCHAR( 3 ) NOT NULL DEFAULT 'off' AFTER `has_zones` ;

	#套餐名称加长
ALTER TABLE `lu_solutions` CHANGE `name` `name` VARCHAR( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE  `lu_solutions` ADD  `cdn_cname` VARCHAR( 88 ) NOT NULL AFTER  `cdn_nodes`;
ALTER TABLE `lu_solutions` ADD `recommend_index` TINYINT( 1 ) NOT NULL AFTER `price` ,
ADD INDEX ( `recommend_index` ) ;
ALTER TABLE `lu_solutions` ADD `default_image` VARCHAR( 45 ) NOT NULL AFTER `recommend_index` ;
ALTER TABLE `lu_solutions` CHANGE `price` `price` FLOAT( 10 ) NOT NULL;

	#流量限制相关
ALTER TABLE `lu_solutions` ADD `host_flow_1M` FLOAT NOT NULL AFTER `host_limit_rate` ,
ADD `host_flow_1Y` FLOAT NOT NULL AFTER `host_flow_1M` ,
ADD `host_flow_1d` FLOAT NOT NULL AFTER `host_flow_1Y` ,
ADD `host_flow_7d` FLOAT NOT NULL AFTER `host_flow_1d` ,
ADD `host_flow_30d` FLOAT NOT NULL AFTER `host_flow_7d` ,
ADD `host_flow_alarm_threshold` FLOAT NOT NULL AFTER `host_flow_30d` ,
ADD `host_limit_conn_moreflows` SMALLINT NOT NULL AFTER `host_flow_alarm_threshold` ,
ADD `host_limit_rate_moreflows` SMALLINT NOT NULL AFTER `host_limit_conn_moreflows`;

ALTER TABLE  `lu_solutions` ADD  `host_proxy_store_mem_size` MEDIUMINT NOT NULL AFTER  `host_php_timeout`;
ALTER TABLE  `lu_solutions` ADD  `host_proxy_store_hd_size` INT NOT NULL AFTER  `host_proxy_store_mem_size`;

	#价格
ALTER TABLE  `lu_solutions` ADD  `price_3Y` FLOAT NOT NULL AFTER  `price` ,
ADD  `price_2Y` FLOAT NOT NULL AFTER  `price_3Y` ,
ADD  `price_1Y` FLOAT NOT NULL AFTER  `price_2Y` ,
ADD  `price_6M` FLOAT NOT NULL AFTER  `price_1Y` ,
ADD  `price_3M` FLOAT NOT NULL AFTER  `price_6M` ,
ADD  `price_1M` FLOAT NOT NULL AFTER  `price_3M` ,
ADD  `price_15d` FLOAT NOT NULL AFTER  `price_1M` ,
ADD  `price_7d` FLOAT NOT NULL AFTER  `price_15d` ,
ADD  `price_3d` FLOAT NOT NULL AFTER  `price_7d` ,
ADD  `price_1d` FLOAT NOT NULL AFTER  `price_3d`;

#-------------lu_users
	#用户表的API接口密码
ALTER TABLE `lu_users` ADD `api_password` VARCHAR( 32 ) NOT NULL AFTER `password` ;
ALTER TABLE `lu_users` ADD INDEX ( `api_password` ) ;

ALTER TABLE `lu_users` ADD `balance` FLOAT NOT NULL AFTER `description` ;

ALTER TABLE `lu_users` ADD `company_name` VARCHAR( 50 ) NOT NULL AFTER `address` ,
ADD `company_address` VARCHAR( 128 ) NOT NULL AFTER `company_name` ,
ADD `company_tel` VARCHAR( 80 ) NOT NULL AFTER `company_address` ;
ALTER TABLE `lu_users` ADD `truename` VARCHAR( 15 ) NOT NULL AFTER `company_tel`;
ALTER TABLE `lu_users` ADD `company_website` VARCHAR( 128 ) NOT NULL AFTER `company_tel` ;

ALTER TABLE `lu_users` ADD `card_number` VARCHAR( 128 ) NOT NULL AFTER `truename` ;
	#清空service@zijidelu.org
UPDATE `LuManager`.`lu_users` SET `email` = '' WHERE `lu_users`.`id` =1520;
	#添加
ALTER TABLE `lu_users` ADD INDEX ( `email` ) ;


#-------------lu_flows
ALTER TABLE `lu_flows` ADD `flowed` VARCHAR(64) NOT NULL AFTER `flow` ;


#-------------lu_dnss
ALTER TABLE `lu_dnss` ADD INDEX ( `view` ) ;
ALTER TABLE `lu_dnss` ADD `status` TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `sort` ;
ALTER TABLE `lu_dnss` ADD INDEX ( `status` ) ;

#-------------lu_hosts
ALTER TABLE `lu_hosts` ADD INDEX ( `proxy_store` ) ;
ALTER TABLE `lu_hosts` ADD `host_mark` VARCHAR( 68 ) NOT NULL AFTER `status` ,
ADD INDEX ( `host_mark` ) ;

ALTER TABLE `lu_hosts` ADD `proxy_store_ip` VARCHAR( 40 ) NOT NULL AFTER `proxy_store` ;

ALTER TABLE `lu_hosts` ADD `proxy_store_cache_time` TEXT NOT NULL AFTER `proxy_store_file_type` ;

ALTER TABLE `lu_hosts` ADD `proxy_store_use_memcached` VARCHAR( 3 ) NOT NULL AFTER `proxy_store_cache_time` ;

ALTER TABLE `lu_hosts` ADD `proxy_store_cache_time_all` SMALLINT NOT NULL AFTER `proxy_store_cache_time` ;

ALTER TABLE `lu_hosts` ADD `img_flash_cache_time` VARCHAR( 10 ) NOT NULL AFTER `upstream_type` ,
ADD `js_css_cache_time` VARCHAR( 10 ) NOT NULL AFTER `img_flash_cache_time` ;

ALTER TABLE  `lu_hosts` ADD  `deny_auto_backup` CHAR( 3 ) NOT NULL AFTER  `dir`;

ALTER TABLE  `lu_hosts` ADD  `gzip` VARCHAR( 3 ) NOT NULL DEFAULT  'on' AFTER  `safe_mode`;

ALTER TABLE  `lu_hosts` ADD  `limit_conn` SMALLINT NOT NULL AFTER  `gzip` ,
ADD  `limit_rate` MEDIUMINT NOT NULL AFTER  `limit_conn`;

ALTER TABLE  `lu_hosts` ADD  `proxy_store_mem_size` SMALLINT NOT NULL AFTER  `proxy_store_index`;

ALTER TABLE  `lu_hosts` ADD  `proxy_store_performance_optimization` TINYINT( 1 ) NOT NULL AFTER  `proxy_store_use_memcached`;

ALTER TABLE  `lu_hosts` ADD  `proxy_store_auto_flush` TINYINT( 1 ) NOT NULL AFTER  `proxy_store_performance_optimization`;

ALTER TABLE  `lu_solutions` ADD  `host_allow_ips` TEXT NOT NULL AFTER  `host_proxy_store_mem_size`;

ALTER TABLE  `lu_hosts` ADD  `cgi_port` MEDIUMINT(6) NOT NULL AFTER  `port`;





#-------------lu_logs
ALTER TABLE `lu_logs` ADD `bk_info` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `event` ,
#ADD `module_name` CHAR( 12 ) NOT NULL AFTER `bk_info` ,
ADD INDEX ( `bk_info` ) ;
#ALTER TABLE `lu_logs` ADD INDEX ( `module_name` );
ALTER TABLE  `lu_logs` CHANGE  `event`  `event` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;


#-------------lu_mysqls
ALTER TABLE `lu_mysqls` ADD `deny_auto_backup` CHAR( 3 ) NOT NULL AFTER `sort` ;















CREATE TABLE IF NOT EXISTS `lu_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` smallint(6) NOT NULL DEFAULT '2000',
  `dateline` int(10) NOT NULL,
  `dateline_update` int(10) NOT NULL,
  `description` varchar(500) NOT NULL,
  `type` varchar(15) NOT NULL,
  `money` float NOT NULL,
  `balance` float NOT NULL,
  `added` varchar(25) NOT NULL,
  `deadline` varchar(10) NOT NULL,
  `pay_method` varchar(30) NOT NULL,
  `trade_no` varchar(18) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `sid` (`sid`),
  KEY `sort` (`sort`),
  KEY `trade_no` (`trade_no`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `lu_cdns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` smallint(6) NOT NULL DEFAULT '2000',
  `dateline` int(10) NOT NULL,
  `dateline_update` int(10) NOT NULL,
  `description` text NOT NULL,
  `domains` varchar(68) NOT NULL,
  `proxy_store_ip` varchar(30) NOT NULL,
  `proxy_store` tinyint(1) NOT NULL,
  `proxy_store_index` varchar(20) NOT NULL,
  `proxy_store_file_type` text NOT NULL,
  `proxy_store_file_type_allowflush` text NOT NULL,
  `proxy_store_cache_time` text NOT NULL,
  `proxy_store_cache_time_all` smallint(6) NOT NULL,
  `proxy_store_use_memcached` varchar(3) NOT NULL,
  `proxy_store_performance_optimization` tinyint(1) NOT NULL,
  `proxy_store_auto_flush` tinyint(1) NOT NULL,
  `proxy_store_delete_password` varchar(32) NOT NULL,
  `host_mark` varchar(68) NOT NULL,
  `error_log` varchar(3) NOT NULL,
  `access_log` varchar(3) NOT NULL,
  `access_log_type` varchar(3) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `sid` (`sid`),
  KEY `sort` (`sort`),
  KEY `domains` (`domains`),
  KEY `host_mark` (`host_mark`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `lu_nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` smallint(6) NOT NULL DEFAULT '2000',
  `name` varchar(32) NOT NULL,
  `dateline` int(10) NOT NULL,
  `dateline_update` int(10) NOT NULL,
  `description` text NOT NULL,
  `api_url` varchar(128) NOT NULL,
  `api_key` varchar(200) NOT NULL,
  `api_user` varchar(25) NOT NULL,
  `api_userpassword` varchar(32) NOT NULL,
  `is_host` tinyint(1) NOT NULL,
  `is_dns` tinyint(1) NOT NULL,
  `is_cdn` tinyint(1) NOT NULL,
  `is_memcached` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `sid` (`sid`),
  KEY `sort` (`sort`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `lu_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` mediumint(9) NOT NULL,
  `sort` smallint(6) NOT NULL DEFAULT '2000',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `dateline` int(10) NOT NULL,
  `dateline_update` int(10) NOT NULL,
  `dateline_expire` int(10) NOT NULL,
  `description` varchar(100) NOT NULL,
  `sid` mediumint(9) NOT NULL,
  `count` smallint(6) NOT NULL,
  `price` float NOT NULL,
  `order_type` tinyint(1) NOT NULL,
  `pay_status` tinyint(1) NOT NULL,
  `trade_no` char(18) NOT NULL,
  `total_fee` float NOT NULL,
  `date_len` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `trade_no` (`trade_no`),
  KEY `uid` (`uid`),
  KEY `sort` (`sort`),
  KEY `status` (`status`),
  KEY `sid` (`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE `lu_settings`;
CREATE TABLE IF NOT EXISTS `lu_settings` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `option_name` varchar(35) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

INSERT INTO `lu_settings` (`id`, `option_name`, `value`) VALUES
(1, 'modules', ''),
(2, 'allow_ip', ''),
(3, 'api_key', ''),
(4, 'login_msg', '自己的路全体工作人员感谢您选择LuManager'),
(5, 'deny_ip_access', '0'),
(6, 'alipay_partner', '208800215190****'),
(7, 'alipay_key', ''),
(8, 'alipay_seller_email', 'service@lumanger.org'),
(9, 'alipay_notify_url', 'http://www.zijidelu.com:8888/Common/alipay/notify_url.php'),
(10, 'alipay_return_url', 'http://www.zijidelu.com:8888/Common/alipay/return_url.php'),
(11, 'alipay_show_url', 'http://www.alipay.com'),
(12, 'alipay_mainname', '自己的路(zijidelu.org)'),
(13, 'backup_ftp_host', '192.168.1.222'),
(14, 'backup_ftp_port', '21'),
(15, 'backup_ftp_user', 'zijidelu'),
(16, 'backup_ftp_password', ''),
(17, 'hosts_backup_time_month', 'N;'),
(18, 'hosts_backup_time_day', 'N;'),
(19, 'hosts_backup_time_hour', 'N;'),
(20, 'hosts_backup_time_minute', ''),
(21, 'mysqls_backup_time_month', 'N;'),
(22, 'mysqls_backup_time_day', 'N;'),
(23, 'mysqls_backup_time_hour', 'N;'),
(24, 'mysqls_backup_time_minute', ''),
(25, 'hosts_backup_incremental_time', '30'),
(26, 'hosts_backup_fully_time', '1440'),
(27, 'mysqls_backup_incremental_time', '30'),
(28, 'mysqls_backup_fully_time', '1440'),
(29, 'admin_email', ''),
(30, 'sendmail_type', '2'),
(31, 'smtp_host', ''),
(32, 'smtp_port', '25'),
(33, 'smtp_auth', '1'),
(34, 'smtp_from', ''),
(35, 'smtp_user', ''),
(37, 'smtp_password', ''),
(38, 'proxy_store_default_file_type', 'html|htm|shtml|shtm|gif|jpg|jpeg|png|bmp|swf|js|css|mp3|mav|doc|dot|xls|pdf|txt|zip|rar|gz|tgz|bz2|tgz|rm|rmvb'),
(39, 'login_info', '自己的路全体工作人员祝您工作愉快'),
(40, 'only_bind_domains', '0'),
(41, 'proxy_store_mem_size', '24'),
(42, 'flows_time_hour', 'N;'),
(43, 'flows_between_time', '30'),
(44, 'sendmail_between_time', '180'),
(45, 'cpu_limit_between_time', '30'),
(46, 'ftps_limit_between_time', '45'),
(47, 'mysqls_limit_between_time', '45'),
(48, 'ftps_backup_between_time', '720'),
(49, 'backup_ftp_zip_password', ''),
(50, 'backup_ftp_delete_between_time', '3day'),
(51, 'new_user_group_id', ''),
(52, 'new_user_fuid', '1520'),
(53, 'ftps_limit_time_hour', '1440'),
(54, 'mysqls_limit_time_hour', '1440'),
(55, 'hosts_index_files', 'index.html index.htm index.shtml index.php'),
(56, 'sendmail_additional_contents', ''),
(61, 'zoneinfo', 'Asia/Shanghai'),
(62, 'proxy_store_hd_size', '51200'),
(63, 'ajax', 'json'),
(64, 'm', 'Settings'),
(65, 'a', 'common_settings'),
(66, '__hash__', 'f5918576becea65325bf14a0d7e7bc08');


