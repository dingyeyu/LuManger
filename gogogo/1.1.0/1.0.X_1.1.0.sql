#将ftp和数据库的字段名加长
ALTER TABLE  `lu_users` CHANGE  `user`  `user` VARCHAR( 25 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE  `lu_ftps` CHANGE  `name`  `name` VARCHAR( 25 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE  `lu_hosts` CHANGE  `name`  `name` VARCHAR( 25 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE  `lu_mysqls` CHANGE  `name`  `name` VARCHAR( 25 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;


#添加api相关字段
ALTER TABLE  `lu_settings` ADD  `api_switch` TEXT NOT NULL AFTER  `LuManager_GID`,
ADD  `api_ip` TEXT NOT NULL AFTER  `api_switch`,
ADD `api_key` TEXT NOT NULL AFTER `api_ip`;

ALTER TABLE  `lu_users` DROP INDEX  `sort_2`;

ALTER TABLE  `lu_solutions` ADD INDEX (  `uid` );
ALTER TABLE  `lu_solutions` ADD INDEX (  `status` );
ALTER TABLE  `lu_solutions` ADD INDEX (  `sort` );
ALTER TABLE  `lu_solutions` ADD INDEX (  `dateline` );

ALTER TABLE `lu_ftps` CHANGE `description` `description` VARCHAR( 500 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `lu_mysqls` CHANGE `description` `description` VARCHAR( 500 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

ALTER TABLE `lu_solutions` ADD `host_limit_conn` SMALLINT NOT NULL AFTER `host_flow` ,
ADD `host_limit_rate` MEDIUMINT NOT NULL AFTER `host_limit_conn` ;

ALTER TABLE `lu_solutions` ADD `host_cgi_port` TEXT NOT NULL AFTER `host_limit_rate` ;
ALTER TABLE `lu_solutions` ADD `host_cpulimit` SMALLINT( 4 ) NOT NULL AFTER `host_cgi_port`;
ALTER TABLE `lu_solutions` ADD `host_max_children` SMALLINT( 3 ) NOT NULL AFTER `host_cgi_port` ;

#继承
ALTER TABLE  `lu_solutions` ADD  `inheritance` TINYINT( 1 ) NOT NULL DEFAULT  '1' AFTER  `sort`;
ALTER TABLE  `lu_solutions` ADD INDEX (  `inheritance` );

#-------------------------lu_hosts表
#添加apache_use_cgi字段
ALTER TABLE `lu_hosts` ADD `apache_use_cgi` VARCHAR( 3 ) NOT NULL DEFAULT 'off' AFTER `use_apache_all`;

ALTER TABLE `lu_hosts` ADD `includes` VARCHAR( 3 ) NOT NULL AFTER `indexes`;

ALTER TABLE `lu_hosts` ADD `valid_referers_domains` TEXT NOT NULL AFTER `apache_use_cgi` ,
ADD `valid_referers_rewrite` VARCHAR( 128 ) NOT NULL AFTER `valid_referers_domains` ;

ALTER TABLE `lu_hosts` ADD `allow_ssl` VARCHAR( 3 ) NOT NULL DEFAULT 'off' AFTER `proxy_pass`;

ALTER TABLE `lu_hosts` ADD `only_ssl` VARCHAR( 3 ) NOT NULL AFTER `allow_ssl` ,
ADD `redirect_url` VARCHAR( 128 ) NOT NULL AFTER  `only_ssl` ,
ADD  `redirect_type` VARCHAR( 3 ) NOT NULL AFTER  `redirect_url`;

ALTER TABLE  `lu_hosts` ADD  `proxy_store` VARCHAR( 3 ) NOT NULL AFTER  `proxy_pass`;
ALTER TABLE `lu_hosts` ADD `proxy_store_file_type` VARCHAR( 200 ) NOT NULL AFTER `proxy_store` ,
ADD `proxy_store_index` VARCHAR( 20 ) NOT NULL AFTER `proxy_store_file_type`;
ALTER TABLE `lu_hosts` ADD `proxy_store_delete_password` VARCHAR( 32 ) NOT NULL AFTER `proxy_store_index`;

ALTER TABLE  `lu_hosts` ADD  `auth_basic_username` VARCHAR( 25 ) NOT NULL AFTER  `access_log_type` ,
ADD  `auth_basic_password` VARCHAR( 50 ) NOT NULL AFTER  `auth_basic_username`;

#nginx的http扩展
ALTER TABLE  `lu_hosts` ADD  `nginx_extends_http` TEXT NOT NULL AFTER  `nginx_extends`;

#负载均衡
ALTER TABLE  `lu_hosts` ADD  `upstream` TEXT NOT NULL AFTER  `nginx_extends_http` ,
ADD  `upstream_type` VARCHAR( 15 ) NOT NULL AFTER  `upstream`;

ALTER TABLE  `lu_hosts` ADD  `nginx_extends_location` TEXT NOT NULL AFTER  `nginx_extends_http`;

#云备份
ALTER TABLE `lu_hosts` ADD `cloud_type` VARCHAR( 20 ) NOT NULL AFTER `proxy_store_delete_password` ,
ADD `cloud_files_suf_number` VARCHAR( 128 ) NOT NULL AFTER `cloud_type` ,
ADD `cloud_target_ip` VARCHAR( 88 ) NOT NULL AFTER `cloud_files_suf_number` ,
ADD `cloud_password` VARCHAR( 32 ) NOT NULL AFTER `cloud_target_ip` ,
ADD INDEX ( `cloud_type` , `cloud_files_suf_number` ) ;

#源数据主机网址
ALTER TABLE `lu_hosts` ADD `cloud_source_url` VARCHAR( 120 ) NOT NULL AFTER `cloud_password` ,
ADD `cloud_description` TEXT NOT NULL AFTER `cloud_source_url` ;
#目标主机网址
ALTER TABLE `lu_hosts` ADD `cloud_target_url` VARCHAR( 120 ) NOT NULL AFTER `cloud_source_url` ;

#主机头
ALTER TABLE  `lu_hosts` ADD  `proxy_pass_host` VARCHAR( 3 ) NOT NULL DEFAULT  'on' AFTER  `proxy_pass`;

#删除cgi端口和进程数
ALTER TABLE `lu_hosts` DROP `cgi_port` ,
DROP `max_children` ;

#ALTER TABLE  `lu_hosts` CHANGE  `cloud_password`  `cloud_password` VARCHAR( 128 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

#-------------------------lu_flows表
#将id字段改成host_id
ALTER TABLE `lu_flows` CHANGE `id` `host_id` MEDIUMINT( 9 ) NOT NULL;

#删除主键
ALTER TABLE `lu_flows` DROP PRIMARY KEY;

#添加id和year_and_month字段
ALTER TABLE `lu_flows` ADD `id` MEDIUMINT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST,
ADD `year_and_month` MEDIUMINT( 6 ) NOT NULL AFTER `id`;
#索引
ALTER TABLE `lu_flows` ADD INDEX ( `year_and_month` );
ALTER TABLE `lu_flows` ADD INDEX ( `host_id` ) ;

ALTER TABLE `lu_flows` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

#-------------------------lu_users表
ALTER TABLE  `lu_users` ADD  `status` TINYINT( 1 ) NOT NULL DEFAULT  '1' AFTER  `password`;
ALTER TABLE  `lu_users` ADD INDEX (  `status` );

#-------------------------lu_groups表
ALTER TABLE  `lu_groups` ADD  `status` TINYINT( 1 ) NOT NULL DEFAULT  '1' AFTER  `name` ,
ADD INDEX (  `status` );

#添加API表
CREATE TABLE `LuManager`.`lu_api` (
`id` SMALLINT NOT NULL ,
`name` VARCHAR( 25 ) NOT NULL ,
`key` VARCHAR( 128 ) NOT NULL ,
`description` VARCHAR( 500 ) NOT NULL 
) ENGINE = MYISAM ;

