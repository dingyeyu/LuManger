#!/bin/sh

. "/usr/local/LuNamp/cmd/common.inc"

cd /tmp

if [ `uname` = 'FreeBSD' ]; then
	#fetch 'http://127.0.0.1:8888/index.php?m=Public&a=flows'
	fetch 'http://127.0.0.1:8888/lu-Public-flows.htm'
else 
	#wget 'http://127.0.0.1:8888/index.php?m=Public&a=flows'
	wget 'http://127.0.0.1:8888/lu-Public-flows.htm'
fi
