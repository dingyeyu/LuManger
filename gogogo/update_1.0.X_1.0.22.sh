#!/bin/sh
#
# ******************************************************************************
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# 作者：		刘新（网名：爱洞特漏）
# 电话：		+86-13688995218
# ------------------------------------++++
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# Author:    Liu Xin
# Tel:       +86-13688995218
# ------------------------------------++++
# Thank you for choosing zijidelu's software!
# ******************************************************************************
#

. './common.inc.sh'


install_LuManager $1 "1.0.22"

cp -R gogogo/1.0.22/* /usr/local/LuNamp/cmd/

cat gogogo/1.0.22/proxy_pass.conf > /usr/local/nginx/conf/proxy_pass.conf

repair_files_permission

lu-restart

if [ -e "/usr/local/LuManager" ]; then
	showmsg "LuManager升级完成，谢谢您的使用!"    "LuManager was successfully updated, Thank you for choose it!"
fi

