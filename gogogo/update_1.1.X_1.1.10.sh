#!/bin/sh
#
# ******************************************************************************
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# 作者：		刘新（网名：爱洞特漏）
# 电话：		+86-13688995218
# ------------------------------------++++
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# Author:    Liu Xin
# Tel:       +86-13688995218
# ------------------------------------++++
# Thank you for choosing zijidelu's software!
# ******************************************************************************
#

. './common.inc.sh'

$group_add lu_bind -g 1505
$user_add lu_bind -u 1505 -g lu_bind -s /sbin/nologin -d /dev/null

#备份数据库
backup_file=/home/mysqls_backup/LuManager_`date +"%Y%m%d%s"`
mkdir -p $backup_file
cp -R /home/mysql_data/LuManager/* $backup_file/

echo "#" > /tmp/update_LuNamp.lock
install_LuNamp $1 "1.1.10" $2 'source'
rm -rf /tmp/update_LuNamp.lock

install_LuManager $1 "1.1.10"

check_mysql_password
/usr/local/mysql/bin/mysql -uroot -p${mysql_password} LuManager < gogogo/1.1.10/1.1.X_1.1.10.sql
sleep 3

repair_files_permission

if [ -e "/usr/local/LuManager" ]; then
	showmsg "LuManager升级完成，谢谢您的使用!"    "LuManager was successfully updated, Thank you for choose it!"
fi

