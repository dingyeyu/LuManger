#!/bin/sh

showmsg () {
	NO3_var=$3
	NO4_var=$4

	if [ ! "$NO3_var" ]; then
		NO3_var=$color_yellow
	fi

	if [ ! "$NO4_var" ]; then
		NO4_var=$color_white
	fi

	echo
	echo -e $NO3_var$1$NO4_var
	if [ "$2" ]; then
		echo -e $NO3_var$2$NO4_var
	fi
	echo -e
}


color_cyan="  [40;36m"
color_red="  [40;31m"
color_yellow="  [40;33m"
color_green="  [40;32m"
color_white="  [40;37m"

if [ `uname` = 'FreeBSD' ]; then
	file_get='fetch'
else
	file_get='wget'
fi


echo $color_green
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "+"
echo "+ [2] USA download mirror"
echo "+ [1] China telecom download mirror"
echo "+ [0] Cancel."
echo "+"
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
showmsg "请选择一个下载点，然后回车！"   "Please select a download mirror and press enter! "
read mirror
echo $color_white

if [ "$mirror" = '1' ]; then
	down="down"
elif [ "$mirror" = '2' ]; then
	down="down3"
elif [ "$mirror" = '3' ]; then
	down="down3"
elif [ "$mirror" = '0' ]; then
	exit;
elif [ "$mirror" = '' ]; then
	down="down"
fi

if [ ! -f "./zijidelu_install.tar.gz" ] && [ ! -d "./zijidelu_install" ] && [ ! -d "../zijidelu_install" ]; then
	showmsg    "开始下载zijidelu_install.tar.gz"    "Starting download zijidelu_install.tar.gz"
	sleep 2
	#`${file_get} http://${down}.zijidelu.org/zijidelu_install.tar.gz`
	
	tar_file='zijidelu_install.tar.gz';
	#$file_get http://lunamp.googlecode.com/files/${tar_file}
	$file_get http://down.zijidelu.org/${tar_file}
	
	file_du='';
	if [ -f "${tar_file}" ]; then
		file_du=`du -s ${tar_file}`
	fi
	if [ ! "`echo $file_du|grep '[0-9]\{2\}'`" ]; then
		rm -f ${tar_file}
		sleep 2;
		$file_get http://down.zijidelu.org/${tar_file}
		file_du2='';
		if [ -f "${tar_file}" ]; then
			file_du2=`du -s ${tar_file}`
		fi
		if [ ! "`echo $file_du2|grep '[0-9]\{2\}'`" ]; then
			rm -f ${tar_file}
			showmsg    "${tar_file}下载失败或不完整"    "Can't to download ${tar_file}"
			exit	
		fi
	fi
	
	if [ ! -e "./zijidelu_install.tar.gz" ]; then
		showmsg    "${tar_file}下载失败"    "Can't to download zijidelu_install.tar.gz"
		exit
	fi
fi

if [ -f zijidelu_install.tar.gz ]; then
	tar -zxvf zijidelu_install.tar.gz
	chmod -R 700 ./zijidelu_install
fi

if [ ! -d "/var/log/LuManager" ]; then
	mkdir -p '/var/log/LuManager'
fi
if [ "$2" = '-y' ] || [ "$2" = '-Y' ]; then
	install_extends='allow_pdo, allow_mysqli';
else
	install_extends='';
fi
echo "$install_extends" > /tmp/install_extends.txt;

if [ -d "zijidelu_install" ]; then
	cd ./zijidelu_install
fi
if [ "$1" = '-y' ] || [ "$1" = '-Y' ]; then
	./i.sh "-y" "${down}"
else
	./i.sh		''	"${down}"
fi


