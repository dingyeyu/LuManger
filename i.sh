#!/bin/sh
#
# ******************************************************************************
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# 作者：		刘新（网名：爱洞特漏）
# 电话：		+86-13688995218
# ------------------------------------++++
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# Author:    Liu Xin
# Tel:       +86-13688995218
# ------------------------------------++++
# Thank you for choosing zijidelu's software!
# ******************************************************************************
#

. './config.inc.sh'
. './common.inc.sh'

#ai_ntpdate

if [ "$down" = '' ]; then
	if [ `echo $2|grep 'down'` ]; then
		down=$2
	else
		echo $color_green
		echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
		echo "+"
		echo "+ [2] USA download mirror"
		echo "+ [1] China telecom download mirror"
		echo "+ [0] Cancel."
		echo "+"
		echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
		showmsg "请选择一个下载点，然后回车！"   "Please select a download mirror and press enter! "
		read mirror
		echo $color_white

		if [ "$mirror" = '1' ]; then
			down="down"
		elif [ "$mirror" = '2' ]; then
			down="down3"
		elif [ "$mirror" = '3' ]; then
			down="down3"
		elif [ "$mirror" = '0' ]; then
			exit;
		elif [ "$mirror" = '' ]; then
			down="down"
		fi
	fi
fi


if [ "$lum_ver" = '' ]; then
	echo $color_green
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo "+"
	echo "+ [2] Install LuManager 2.1.2"
	echo "+ [1] Install LuManager 1.1.10"
	echo "+ [0] Cancel."
	echo "+"
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	showmsg "请输入您的选择，然后回车！"   "Please choosing your option and press enter! "
	read install_num
	echo $color_white

	if [ "$install_num" = '1' ]; then
		lum_ver="1.1.10"
		echo "1.1.10" > "/var/tmp/lum_ver.txt";
	elif [ "$install_num" = '2' ]; then
		lum_ver="2.1.2"
		echo "2.1.2" > "/var/tmp/lum_ver.txt";
	elif [ "$install_num" = '' ]; then
		lum_ver="2.1.2"
		echo "2.1.2" > "/var/tmp/lum_ver.txt";
	elif [ "$install_num" = '0' ]; then
		exit;
	fi
fi

#show_msg "Do you need Chinese language tips?[Y/n]"
#read need_chinese
#if [ "$need_chinese" != 'n' ] && [ "$need_chinese" != 'N' ]; then	
#fi

if [ ! -d '/home' ]; then
	if [ ! -d '/usr/home/' ]; then
		mkdir /usr/home
	fi
	ln -s /usr/home /home
fi

user_and_group_add 2>&1 > /dev/null

install_LuNamp $down $lum_ver $1

install_LuManager $down $lum_ver

check_mysql_password

import_data

repair_files_permission

if [ -d '/home/ftp/1501' ]; then
	mv  /home/ftp/1501  /home/ftp/1520

	chown -R zijidelu:LuManager /home/ftp/1520
	chown 770 /home/ftp/1520
	chown -R 775 /home/ftp/1520/*
fi

#改回原密码
if [ "$mysql_password" != 'zijidelu' ]; then
	#mysql密码
	if [ "${mysql_password}" ]; then
		str_replace "'zijidelu'"     "'${mysql_password}'"    "/usr/local/LuManager/Conf/config.php"    "rm_backup_file"
	fi
fi

if [ "`cat /usr/local/LuManager/Conf/config.php | grep -c 'pgsql_password'`" ]; then
	str_replace "'pgsql_password'"     "'zijidelu'"    "/usr/local/LuManager/Conf/config.php"    "rm_backup_file"
fi

if [ -f "/var/tmp/LuNamp_install_from_script.lock" ]; then
	rm -f "/var/tmp/LuNamp_install_from_script.lock";
fi

if [ -e "$LuManager_root/Common/" ]; then
	showmsg "LuManager安装完成，谢谢您的使用!"    "LuManager was successfully installed, Thank you for choose it!"
	showmsg '请使用http://IP:8888或http://domain:8888访问后台管理系统（IP就是您的服务器IP，domain即是指向该IP的域名）。'    'Please use "http://your ip:8888" or "http://your domain:8888" and login LuManager to manager your system!'
	showmsg '为保证所有程序都能正常使用，需重启系统，立即重启吗？[Y/n]'        'In order to LuManager normal operation, You must reboot the system, reboot now?[Y/n]'
	echo
	read reboot_now;
	if [ "$reboot_now" != 'n' ] && [ "$reboot_now" != 'N' ]; then
		reboot
	fi
fi
