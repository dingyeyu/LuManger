#!/bin/sh
#
# ******************************************************************************
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# 作者：		刘新（网名：爱洞特漏）
# 电话：		+86-13688995218
# ------------------------------------++++
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# Author:    Liu Xin
# Tel:       +86-13688995218
# ------------------------------------++++
# Thank you for choosing zijidelu's software!
# ******************************************************************************
#

. './config.inc.sh'
. './common.inc.sh'

#check_mysql_password

#ai_ntpdate

if [ `echo $2|grep 'down'` ]; then
	down=$2
else
	echo $color_green
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo "+"
	echo "+ [2] USA download mirror"
	echo "+ [1] China telecom download mirror"
	echo "+ [0] Cancel."
	echo "+"
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	showmsg "请选择一个下载点，然后回车！"   "Please select a download mirror and press enter! "
	read mirror
	echo $color_white

	if [ "$mirror" = '1' ]; then
		down="down"
		#echo "nameserver 202.96.199.133" > /etc/resolv.conf
		#echo "nameserver 202.106.0.20" >> /etc/resolv.conf
		#echo "nameserver 202.96.0.133" >> /etc/resolv.conf
	elif [ "$mirror" = '2' ]; then
		down="down3"
		#echo "nameserver 208.67.222.222" > /etc/resolv.conf
		#echo "nameserver 8.8.8.8" >> /etc/resolv.conf
		#echo "nameserver 202.96.0.133" >> /etc/resolv.conf
	elif [ "$mirror" = '3' ]; then
		down="down3"
		#echo "nameserver 208.67.222.222" > /etc/resolv.conf
		#echo "nameserver 8.8.8.8" >> /etc/resolv.conf
		#echo "nameserver 202.96.0.133" >> /etc/resolv.conf
	elif [ "$mirror" = '0' ]; then
		exit;
	elif [ "$mirror" = '' ]; then
		down="down"
	fi
fi

#先获取down值
echo $color_green
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "+"
echo "+ [6] Update LuManager 2.X to 2.1.2"
echo "+ [5] Update LuManager 1.1.X to 2.1.2"
echo "+ [4] Update LuManager 1.1.X to 1.1.10"
echo "+ [3] Update LuManager 1.1alpha1, 1.1beta1 to 1.1.0"
echo "+ [2] Update LuManager 1.0.X to 1.1"
echo "+ [1] Update LuManager 1.0stable to 1.0.22"
echo "+ [0] Cancel."
echo "+"
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
showmsg "请输入您的选择，然后回车！"   "Please choosing your option and press enter! "
read install_num
echo $color_white

if [ "$install_num" = '1' ]; then
	./gogogo/update_1.0.X_1.0.22.sh $down $1
elif [ "$install_num" = '2' ]; then
	./gogogo/update_1.0.X_1.1.0.sh $down $1
elif [ "$install_num" = '3' ]; then
	./gogogo/update_1.1.0alpha1_1.1.0.sh $down $1
elif [ "$install_num" = '4' ]; then
	./gogogo/update_1.1.X_1.1.10.sh $down $1
elif [ "$install_num" = '5' ]; then
	./gogogo/update_1.1.X_2.0.X.sh $down $1
elif [ "$install_num" = '6' ]; then
	./gogogo/update_2.X_2.0.X.sh $down $1
elif [ "$install_num" = '' ]; then
	exit;
fi

#改回原密码
if [ "$mysql_password" != 'zijidelu' ]; then
	if [ "${mysql_password}" ]; then
		str_replace "'zijidelu'"     "'${mysql_password}'"    "/usr/local/LuManager/Conf/config.php"    "rm_backup_file"
	fi
fi

if [ "`cat /usr/local/LuManager/Conf/config.php | grep -c 'pgsql_password'`" ]; then
	str_replace "'pgsql_password'"     "'zijidelu'"    "/usr/local/LuManager/Conf/config.php"    "rm_backup_file"
fi

if [ -f "/var/tmp/LuNamp_install_from_script.lock" ]; then
	rm -f "/var/tmp/LuNamp_install_from_script.lock";
fi


