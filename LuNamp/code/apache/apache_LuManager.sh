#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

if [ -e "./common/common.sh" ]; then
	. "./common/common.sh"
else
	. "../common/common.sh"
fi


soft_root="/usr/local/apache_LuManager"
soft_version="2.2.22"
if [ "$1" != '' ]; then
	soft_version=$1;
fi

if [ "${soft_version}" \> '2.2.0' ]; then
	install_func "apr"    "/usr/local/${ext_soft_dir}apr"    "${i_code_root}/apache/apr.sh"
	install_func "apr-util"    "/usr/local/${ext_soft_dir}apr-util"    "${i_code_root}/apache/apr-util.sh"
	install_func "pcre"    "/usr/local/${ext_soft_dir}pcre"    "${i_code_root}/apache/pcre.sh"
fi

cd ${i_soft_root}

rm -rf httpd-${soft_version}
tar -zxvf httpd-${soft_version}.tar.gz
cd httpd-$soft_version

extends='';
if [ "${soft_version}" \> '2.2.0' ]; then
	extends="--with-apr=/usr/local/${ext_soft_dir}apr --with-apr-util=/usr/local/${ext_soft_dir}apr-util";
fi

lun_debug_func "start";

./configure --prefix=$soft_root ${extends} \
--with-z=/usr/local/${ext_soft_dir}zlib \
-with-mpm=prefork \
--enable-rewrite \
--with-port=8888 \
--enable-so \

#--with-ssl=/usr/local/${ext_soft_dir}openssl \
#--enable-ssl
#--with-mpm=worker
make
make install

lun_debug_func "end"

if [ ! `grep -l 'DocumentRoot /usr/local/LuNamp' $soft_root/conf/httpd.conf` ]; then
	cat "${i_code_root}/apache/httpd.conf_add_LuManager.conf" >> $soft_root/conf/httpd.conf
fi

if [ `uname` = 'FreeBSD' ] && [ ! `grep -l 'accf_http_load="yes"' "/boot/loader.conf"` ]; then
	echo 'accf_http_load="yes"' >> "/boot/loader.conf";
fi

str_replace "User daemon/User LuManager"    "$soft_root/conf/httpd.conf"
str_replace "Group daemon/Group LuManager"    "$soft_root/conf/httpd.conf"

cd ${i_soft_root}
rm -rf httpd-$soft_version
cd ${i_code_root}


