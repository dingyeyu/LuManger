#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

if [ -e "./common/common.sh" ]; then
	. "./common/common.sh"
else
	. "../common/common.sh"
fi

soft_root="/usr/local/php_LuManager"
soft_version="5.2.17"

#if [ -e "$soft_root" ]; then
#	echo "您需要删除旧文件后再重装吗？";
#	echo "Do you want to delete old files and then reinstall? [y/n]";
#	read completely_reinstall
#	if [ "$completely_reinstall" = 'Y' ] || [ "$completely_reinstall" = 'y' ]; then
#		rm -rf $soft_root
#	fi
#fi

cd ${i_soft_root}

rm -rf php-${soft_version}
tar -zxvf php-${soft_version}.tar.gz
cd php-${soft_version}

if [ "${linux_type}" != 'RedHat' ] || [ "$lun_debug" = "1" ]; then
	extends="--with-gd --with-gdbm=/usr/local/${ext_soft_dir}gdbm --with-freetype-dir=/usr/local/${ext_soft_dir}freetype --with-jpeg-dir=/usr/local/${ext_soft_dir}jpeg --with-png-dir=/usr/local/${ext_soft_dir}libpng"
else
	extends='--with-gd --with-freetype-dir --with-jpeg-dir --with-png-dir'
fi

if [ ${enable_pdo_mysql} = 'yes' ] || [ "$3" = '' ]; then
	extends="--with-pdo-mysql=/usr/local/mysql/bin/mysql_config  --enable-pdo ${extends}";
fi

if [ ${enable_mysqli} = 'yes' ] || [ "$3" = '' ]; then
	extends="--with-mysqli=/usr/local/mysql/bin/mysql_config ${extends}";
fi

if [ "$3" \> '1' ]; then
	extends="--without-openssl ${extends}";
else
	extends="--with-openssl ${extends}";
fi

if [ "$3" \> '2' ]; then
	extends="--without-libxml ${extends}";
else
	extends="--with-libxml-dir=/usr/local/${ext_soft_dir}libxml ${extends}";
fi

if [ "$lun_debug" != "1" ]; then
	extends="--enable-inline-optimization ${extends}"
fi

lun_debug_func "start"

./configure --prefix=$soft_root ${extends} \
--with-apxs2=/usr/local/apache_LuManager/bin/apxs \
--with-mysql=/usr/local/mysql \
--with-pgsql=/usr/local/pgsql \
--disable-debug \
--enable-mbstring \
--with-curl=/usr/local/curl \
--with-zlib=/usr/local/${ext_soft_dir}zlib \
--with-ttf \
--enable-gd-native-ttf \
--enable-gd-jis-conv \
--with-iconv=/usr/local/${ext_soft_dir}libiconv \
--enable-ftp \
--with-curlwrappers \
--enable-sockets \
--with-mcrypt=/usr/local/${ext_soft_dir}libmcrypt \
--enable-zip \
--enable-bcmath \
--enable-calendar \
--enable-exif
#--with-zlib-dir=/usr/local/${ext_soft_dir}zlib \

/usr/local/${ext_soft_dir}make/bin/make ZEND_EXTRA_LIBS='-liconv';
/usr/local/${ext_soft_dir}make/bin/make install
if [ ! -d "$soft_root" ]; then
	/usr/local/${ext_soft_dir}make/bin/make install
fi

lun_debug_func "end"

#--with-pdo-mysql=/usr/local/mysql/bin/mysql_config \
#--with-mysqli=/usr/local/mysql/bin/mysql_config \
#--with-mhash \
#--enable-embedded-mysqli \
#--with-pdo-mysql=/usr/local/mysql/bin/mysql_config \


if [ -f "./php.ini-dist" ]; then
	cat php.ini-dist > $soft_root/lib/php.ini
elif [ -f "./php.ini-production" ]; then
	cat php.ini-production > $soft_root/lib/php.ini
fi

if [ -f "$soft_root/lib/php.ini" ]; then
	chown LuManager $soft_root/lib/php.ini
else
	if [ "$3" = '' ]; then
		${i_code_root}/php/$0 "$1" "$2" '2';
	elif [ "$3" = '2' ]; then
		${i_code_root}/php/$0 "$1" "$2" '3';
	elif [ "$3" = '3' ]; then
		${i_code_root}/php/$0 "$1" "$2" '4';
	fi
	echo "$soft_root/lib/php.ini 不存在！";
	exit;
fi

str_replace     "disable_functions =/disable_functions = chroot, popen, get_defined_functions, get_defined_constants"    "$soft_root/lib/php.ini"
str_replace     "disable_classes =/disable_classes = get_object_vars, get_class_methods, get_class_vars"    "$soft_root/lib/php.ini"

cd ${i_soft_root}
rm -rf php-${soft_version}
cd ${i_code_root}


echo $enter4
${i_code_root}/php/eaccelerator_apache_LuManager.sh

#echo $enter4
#${i_code_root}/php/memcache_apache_LuManager.sh


if [ ! `grep -l '\[Zend\]' "$soft_root/lib/php.ini"` ]; then
	cat "${i_code_root}/php/zend_LuManager_php.ini_add.conf" >> "$soft_root/lib/php.ini"	
fi
