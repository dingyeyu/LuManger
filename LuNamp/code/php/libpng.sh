#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

if [ -e "./common/common.sh" ]; then
	. "./common/common.sh"
else
	. "../common/common.sh"
fi


soft_root="/usr/local/${ext_soft_dir}libpng"
soft_version="1.5.12"
if [ "$1" != '' ]; then
	soft_version=$1;
fi

cd ${i_soft_root}


tar -zxvf libpng-${soft_version}.tar.gz
cd libpng-${soft_version}

lun_debug_func "start"

./configure --prefix=$soft_root --with-zlib-prefix=/usr/local/${ext_soft_dir}zlib
make
make install

lun_debug_func "end"

cd ${i_soft_root}
rm -rf libpng-${soft_version}
cd ${i_code_root}