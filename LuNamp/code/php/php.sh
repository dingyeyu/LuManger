#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

if [ -e "./common/common.sh" ]; then
	. "./common/common.sh"
else
	. "../common/common.sh"
fi

soft_root="/usr/local/php"
soft_version="";#请填入软件版本号，如5.1.19。如果留空，则使用config.sh或lun_install_config.sh（在zijidelu_install目录下，相对config.sh有优先权）中的配置值
if [ "$soft_version" = '' ]; then
	soft_version="${php_ver}"
fi
#fpm_version="0.5.14"

cd ${i_soft_root}

rm -rf php-${soft_version}
tar -zxvf php-${soft_version}.tar.gz
#gzip -cd php-${soft_version}-fpm-${fpm_version}.diff.gz | patch -d php-${soft_version} -p1
cd php-${soft_version}

if [ "${linux_type}" != 'RedHat' ] || [ "$lun_debug" = "1" ]; then
	extends="--with-gd --with-gdbm=/usr/local/${ext_soft_dir}gdbm --with-freetype-dir=/usr/local/${ext_soft_dir}freetype --with-jpeg-dir=/usr/local/${ext_soft_dir}jpeg --with-png-dir=/usr/local/${ext_soft_dir}libpng"
else
	extends='--with-gd --with-freetype-dir --with-jpeg-dir --with-png-dir'
fi

if [ ${enable_pdo_mysql} = 'yes' ] || [ "$3" = '' ]; then
	extends="--with-pdo-mysql=/usr/local/mysql/bin/mysql_config  --enable-pdo ${extends}";
fi

if [ ${enable_mysqli} = 'yes' ] || [ "$3" = '' ]; then
	extends="--with-mysqli=/usr/local/mysql/bin/mysql_config ${extends}";
fi

if [ "$3" \> '1' ]; then
	extends="--without-openssl ${extends}";
else
	extends="--with-openssl ${extends}";
fi

if [ "$3" \> '2' ]; then
	extends="--without-libxml ${extends}";
else
	extends="--with-libxml-dir=/usr/local/${ext_soft_dir}libxml ${extends}";
fi

if [ "`echo $soft_version|grep '5.2.'`" ]; then
	extends="--with-ttf ${extends}";
fi

if [ "$lun_debug" != "1" ]; then
	extends="--enable-inline-optimization ${extends}"
fi

lun_debug_func "start"

./configure --prefix=$soft_root ${extends} \
--with-apxs2=/usr/local/apache/bin/apxs \
--with-mysql=/usr/local/mysql \
--with-pgsql=/usr/local/pgsql \
--disable-debug \
--enable-mbstring \
--with-zlib=/usr/local/${ext_soft_dir}zlib \
--with-curl=/usr/local/curl \
--enable-gd-native-ttf \
--enable-gd-jis-conv \
--with-iconv=/usr/local/${ext_soft_dir}libiconv \
--enable-ftp \
--with-curlwrappers \
--enable-sockets \
--with-mcrypt=/usr/local/${ext_soft_dir}libmcrypt \
--enable-zip \
--enable-bcmath \
--enable-calendar \
--enable-exif \
--enable-soap
#--with-zlib-dir=/usr/local/${ext_soft_dir}zlib \

/usr/local/${ext_soft_dir}make/bin/make ZEND_EXTRA_LIBS='-liconv';
echo
/usr/local/${ext_soft_dir}make/bin/make install

lun_debug_func "end"

#--enable-fpm --enable-fastcgi --enable-force-cgi-redirect --enable-discard-path
#--with-mysqli=/usr/local/mysql/bin/mysql_config \
#--with-pdo-mysql=/usr/local/mysql/bin/mysql_config \
#--with-mhash \
#--enable-embedded-mysqli \

if [ ! -d "$soft_root" ]; then
	/usr/local/${ext_soft_dir}make/bin/make install
fi

#$soft_root/bin/php ${i_soft_root}/go-pear.phar

if [ ! -f "$soft_root/lib/php.ini" ]; then
	if [ -f "./php.ini-dist" ]; then
		cat php.ini-dist > $soft_root/lib/php.ini
	elif [ -f "./php.ini-production" ]; then
		cat php.ini-production > $soft_root/lib/php.ini
	fi
fi

if [ -f "$soft_root/lib/php.ini" ]; then
	chown LuManager $soft_root/lib/php.ini
else
	if [ "$3" = '' ]; then
		${i_code_root}/php/$0 "$1" "$2" '2';
	elif [ "$3" = '2' ]; then
		${i_code_root}/php/$0 "$1" "$2" '3';
	elif [ "$3" = '3' ]; then
		${i_code_root}/php/$0 "$1" "$2" '4';
	fi
	echo "$soft_root/lib/php.ini 不存在，没安装成功！";
	exit;
fi

cat ${i_code_root}/php/php-fpm.conf > $soft_root/etc/php-fpm.conf

#str_replace     "disable_functions =/disable_functions = shell_exec, system, passthru, exec, popen, proc_open"    "$soft_root/lib/php.ini"

cd .. 
rm -rf php-${soft_version}
cd ${i_code_root}

if [ -d "${soft_root}" ]; then
	echo $enter4
	${i_code_root}/php/eaccelerator_apache.sh
	
	echo $enter4
	${i_code_root}/php/memcache_apache.sh
fi

