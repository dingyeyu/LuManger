#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

if [ -e "./common/common.sh" ]; then
	. "./common/common.sh"
else
	. "../common/common.sh"
fi

if [ `uname` = 'FreeBSD' ] && [ "$system_ver" != '6' ]; then
	cp -R ${i_soft_root}/lib_files_${system_bit}/* /lib/
	if [ ! `grep -l 'compat6x_enable' "/etc/rc.conf"` ]; then
		echo 'compat6x_enable="YES"' >> /etc/rc.conf
	fi
fi

cp -R ${i_soft_root}/Zend_${system_bit}_${uname} /usr/local/Zend
cp -R ${i_soft_root}/Zend_${system_bit}_${uname} /usr/local/Zend_LuManager

cd ${i_code_root}
