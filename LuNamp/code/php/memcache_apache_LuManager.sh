#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#
 
if [ -e "./common/common.sh" ]; then
	. "./common/common.sh"
else
	. "../common/common.sh"
fi


soft_version="2.2.6"
if [ "$1" != '' ]; then
	soft_version=$1;
fi
php_path="/usr/local/php_LuManager";

cd ${i_soft_root}

rm -rf memcache-${soft_version}
tar -zxvf memcache-${soft_version}.tgz
cd memcache-${soft_version}

make clean
$php_path/bin/phpize
./configure --enable-memcache \
--with-php-config=$php_path/bin/php-config \
--with-zlib-dir

make
make install

if [ ! `grep -l '\[memcache\]' "$php_path/lib/php.ini"` ]; then
	#cat "${i_code_root}/php/php.ini_php_LuManager_memcache_add.conf" >> "$php_path/lib/php.ini"

	ext_path=`find $php_path/lib/php/extensions/ -name "*-*-*-*" | grep "^$php_path" | awk -F / '{print substr($0, match($0, "extensions/")+11)}'`
	str_replace "__ext_dir__/$ext_path"	"${i_code_root}/php/php.ini_php_LuManager_memcache_add.conf"
	
	cat "${i_code_root}/php/php.ini_php_LuManager_memcache_add.conf" >> "$php_path/lib/php.ini"
fi

cd ${i_soft_root}
rm -rf memcache-${soft_version}
cd ${i_code_root}

