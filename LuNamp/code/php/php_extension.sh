#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#
 
if [ -e "./common/common.sh" ]; then
	. "./common/common.sh"
else
	. "../common/common.sh"
fi

extension_name='pdo_mysql';	#扩展名称
php_version="5.2.17";	#PHP版本号
php_path="/usr/local/php_fcgi";	#PHP所在目录（/usr/local/php或者/usr/local/php_fcgi）

if [ "$1" ]; then
	extension_name="$1"
fi

if [ "$2" ]; then
	php_version="$2"
fi

if [ "$3" ]; then
	php_path="$3"
fi

cd ${i_soft_root}

rm -rf php-${php_version}
tar -zxvf php-${php_version}.tar.gz
cd php-${php_version}/ext/${extension_name}

make clean
$php_path/bin/phpize
./configure --with-php-config=$php_path/bin/php-config $4

make
make install

if [ ! `grep -l "\[${extension_name}\]" "$php_path/lib/php.ini"` ]; then
	ext_path=`find $php_path/lib/php/extensions/ -name "*-*-*-*" | grep "^$php_path" | awk -F / '{print substr($0, match($0, "extensions/")+11)}'`
	
	echo '' >> $php_path/lib/php.ini
	echo "[${extension_name}]" >> $php_path/lib/php.ini
	echo "extension_dir=\"$php_path/lib/php/extensions/$ext_path\"" >> $php_path/lib/php.ini
	echo "extension = \"${extension_name}.so\"" >> $php_path/lib/php.ini
	echo '' >> $php_path/lib/php.ini
fi

cd ${i_soft_root}
rm -rf php-${php_version}
cd ${i_code_root}


