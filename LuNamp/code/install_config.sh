#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#


#---------------------说明：
# 如果LuNamp目录放置在zijidelu_install目录下，并且zijidelu_install目录下存在lun_install_config.sh文件
# 则使用lun_install_config.sh中的配置，此文件将失效，也就是说lun_install_config.sh文件中的配置具有优先权
#
#

#安装Apache
enable_apache='yes'

#安装DNS服务器服务程序bind
enable_bind='yes'
#bind所使用的数据库类型
bind_database_type='mysql'

#PHP版本（非FastCGI）
#No FastCGI, 5.3.14 or 5.2.17
php_ver='5.3.14'
#FreeBSD只能支持PHP5.2.X
if [ `uname` = 'FreeBSD' ]; then
	php_ver='5.2.17'
fi

#MySQL版本
#5.5.24 or 5.1.69
mysql_ver='5.1.69'

enable_pdo_mysql='no'
enable_mysqli='no'

#0.9.5.3 or 0.9.6.1
eaccelerator_ver='0.9.6.1'
eaccelerator_ver_FastCGI='0.9.6.1'
