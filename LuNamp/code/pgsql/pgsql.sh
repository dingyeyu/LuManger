#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

if [ -e "./common/common.sh" ]; then
	. "./common/common.sh"
else
	. "../common/common.sh"
fi

soft_root="/usr/local/pgsql"
soft_version="9.1.4"
if [ "$1" != '' ]; then
	soft_version=$1;
fi

cd ${i_soft_root}

kill_soft "postgres"

rm -rf postgresql-${soft_version}
tar -zxvf postgresql-${soft_version}.tar.gz
cd postgresql-${soft_version}

./configure --prefix=${soft_root} --without-zlib ${pgsql_debug}
/usr/local/${ext_soft_dir}make/bin/make
/usr/local/${ext_soft_dir}make/bin/make install;

$group_add postgres -g 1506
$user_add postgres -u 1506 -g postgres -d /dev/null

mkdir ${pgsql_data_path}
chown -R postgres:postgres ${pgsql_data_path}
chmod -R 0700 ${pgsql_data_path}
if [ -f "${pgsql_data_path}/pgsql.log" ]; then
	rm -rf ${pgsql_data_path}/*
fi
su postgres -c "${soft_root}/bin/initdb -D ${pgsql_data_path}"
chmod -R 0700 ${pgsql_data_path}

echo '#' > ${pgsql_data_path}/pgsql.log;
chown postgres ${pgsql_data_path}/pgsql.log
#su postgres -c "${soft_root}/bin/postgres -D ${pgsql_data_path} >/home/pgsql_data/pgsql.log 2>&1 &"
su postgres -c "${soft_root}/bin/pg_ctl -D ${pgsql_data_path} -l /home/pgsql_data/pgsql.log start"
chmod -R 0700 ${pgsql_data_path}
sleep 3;
su postgres -c "${soft_root}/bin/pg_ctl -D ${pgsql_data_path} -l /home/pgsql_data/pgsql.log start"
chmod -R 0700 ${pgsql_data_path}

sleep 5;
${soft_root}/bin/createdb -T template0 -U postgres -E UTF8 LuManager
#${soft_root}/bin/psql test
chmod -R 0700 ${pgsql_data_path}

if [ `ps -Af | grep "postgres" | grep -vc 'grep'` != '0' ]; then
	${soft_root}/bin/psql -U postgres -d LuManager < ${i_code_root}/pgsql/pgsql_data.sql.conf
else
	showmsg "PgSQL数据导入不成功，可能是数据库没启动，或者是PgSQL没安装成功，请重装一次试试！"    "PgSQL data was not imported！"	 $color_red	$color_red
	echo $color_white;
	exit;
fi
sleep $sleep_time

chmod -R 0700 ${pgsql_data_path}
$soft_root/bin/psql -U postgres -c "alter role postgres password 'zijidelu';"
chmod -R 0700 ${pgsql_data_path}

ln -sf $soft_root/lib/* /usr/lib/
ln -s $soft_root/include/* /usr/include/

cat ${i_code_root}/pgsql/pg_hba.conf > ${pgsql_data_path}/pg_hba.conf

if [ `uname` = 'Linux' ]; then
	export LD_LIBRARY_PATH="${ld_lib_path}"

	if [ ! `grep -l "$soft_root/lib"    '/etc/ld.so.conf'` ]; then
		echo "$soft_root/lib" >> /etc/ld.so.conf
	fi
	ldconfig
fi

ln -s ${soft_root}/bin/* $LuNamp_root/other_cmd/

cd ${i_soft_root}
rm -rf postgresql-${soft_version}
cd ${i_code_root}


