#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

if [ -e "./common/common.sh" ]; then
	. "./common/common.sh"
else
	. "../common/common.sh"
fi

LuNamp_wwwroot="/home/ftp/LuNamp"

mkdir -p ${LuNamp_wwwroot}
cat ${i_code_root}/LuNamp/phpinfo.php > ${LuNamp_wwwroot}/phpinfo.php
cat ${i_code_root}/LuNamp/index.php > ${LuNamp_wwwroot}/index.php
#cat ${i_code_root}/LuNamp/index.html > ${LuNamp_wwwroot}/index.html

chown -R daemon:daemon ${LuNamp_wwwroot}
chmod -R 751 ${LuNamp_wwwroot}

if [ -f "$LuNamp_root/cmd/lu-start" ]; then
	rm -rf $LuNamp_root/cmd/*
fi
if [ ! -d "${LuNamp_root}/cmd" ]; then
	mkdir -p ${LuNamp_root}/cmd
fi

cp -R ${i_code_root}/cmd/* $LuNamp_root/cmd/
chown root:LuManager $LuNamp_root/cmd/*
chmod -R 570 $LuNamp_root/cmd/*
