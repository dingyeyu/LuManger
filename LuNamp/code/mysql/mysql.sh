#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

#本脚本可以带两个参数，即./mysql.sh "参数1" "参数2"
#第1个参数值如果为-y，即代表升级mysql
#第2个参数的值是升级数据库时的数据库版本号

if [ -e "./common/common.sh" ]; then
	. "./common/common.sh"
else
	. "../common/common.sh"
fi

soft_root="/usr/local/mysql"

soft_version="";#请填入软件版本号，如5.1.19。如果留空，则使用install_config.sh或lun_install_config.sh（在zijidelu_install目录下，相对install_config.sh有优先权）中的配置值
if [ "$2" = '-y' ] && [ "$1" != '' ]; then
	soft_version="$1"
fi
if [ "$soft_version" = '' ]; then
	soft_version="$mysql_ver"
fi

#ls -r /root/LuNamp/soft|grep 'mysql.*tar'|head -1

cd ${i_soft_root}

rm -rf mysql-${soft_version}
if [ ! -f "mysql-${soft_version}.tar.gz" ]; then
	showmsg "正在下载mysql-${soft_version}.tar.gz"    "Downloading mysql-${soft_version}.tar.gz now..."
	${my_wget} http://downloads.mysql.com/archives/mysql-5.5/mysql-${soft_version}.tar.gz
fi
tar -zxvf mysql-${soft_version}.tar.gz
cd mysql-${soft_version}

if [ -f 'configure' ]; then
	mysql_ver=51
	./configure --prefix=$soft_root \
	--with-extra-charsets=all \
	--enable-thread-safe-client \
	--without-debug \
	--with-charset=utf8 \
	--with-big-tables \
	--with-mysqld-ldflags=-all-static \
	--with-plugins=partition,innobase,archive,blackhole,csv,federated
else
	mysql_ver=55
	/usr/local/${ext_soft_dir}cmake/bin/cmake .
fi

make
make install;

mysql_user='lu_mysql'

kill_soft "mysqld"
echo
sleep 5

if [ -e '/etc/mysql' ]; then
	mv /etc/mysql /etc/mysql_old
fi

if [ -f "$soft_root/share/mysql/my-small.cnf" ]; then
	cat $soft_root/share/mysql/my-small.cnf > /etc/my.cnf
elif [ -f "$soft_root/support-files/my-small.cnf" ]; then
	cat $soft_root/support-files/my-small.cnf > /etc/my.cnf
	if [ `uname` = 'Linux' ] && [ "`grep -c 'loose-skip-innodb'	'/etc/my.cnf'`" -eq '0' ]; then
		sed -i "s/\[mysqld\]/\[mysqld\]\nloose-skip-innodb\ndefault-storage-engine=MyISAM/g" /etc/my.cnf
	fi
else
	showmsg '未找到my.cnf源文件'    'my.cnf is not exists!'
	exit;
fi
ln -sf /etc/my.cnf $soft_root/my.cnf

#if [ ! `grep -l    "datadir=${mysql_data_path}"    "/etc/my.cnf"` ]; then
#	sed '/^#/d;/^$/d;/\[mysqld\]/G' /etc/my.cnf | sed 's/^$/datadir=\/home\/mysql_data/1' > /tmp/my.cnf
#	if [ `grep -l    'datadir'    '/tmp/my.cnf'` ]; then
#		cat /tmp/my.cnf > /etc/my.cnf
#	fi
#fi

chown -R ${mysql_user}:${mysql_user} $soft_root/.

mkdir -p $soft_root/var
chown -R ${mysql_user}:${mysql_user} $soft_root/var
chmod -R 2770 $soft_root/var

datadir=${mysql_data_path}
mkdir -p $datadir
chown -R ${mysql_user}:${mysql_user} $datadir
chmod -R 2770 $datadir

#如果添加-y参数，则为升级数据库
if [ "$2" != '-y' ]; then
	if [ "$mysql_ver" != '51' ]; then
		mysql_install_db=$soft_root/scripts/mysql_install_db
	else
		mysql_install_db=$soft_root/bin/mysql_install_db
	fi
	sh $mysql_install_db --user=${mysql_user} --basedir=$soft_root --datadir=$datadir

	sleep 2

	if [ ! -d "$datadir/mysql" ]; then
		sh $mysql_install_db --user=root --basedir=$soft_root --datadir=$datadir;
		#sh $mysql_install_db --user=${mysql_user} --basedir=$soft_root --datadir=$datadir;
		
		#见鬼!!!!
		if [ ! -d "$datadir/mysql" ]; then
			showmsg    "MySQL初始化失败"    'The mysql_install_db was failed';
			exit;
		fi
	fi	
fi

chown -R root $soft_root/.

chown -R ${mysql_user}:${mysql_user} $datadir
chmod -R 2770 $datadir

sh $soft_root/bin/mysqld_safe --user=${mysql_user} --datadir=$datadir --pid_file=${data_dir}/`hostname`.pid &
sleep 2

if [ "$mysql_ver" != '51' ]; then
	ln -sf $soft_root/lib/* /usr/lib/
	ln -s $soft_root/include/* /usr/include/
else
	ln -sf $soft_root/lib/mysql/* /usr/lib/
	ln -s $soft_root/include/mysql/* /usr/lib/

	ln -s $soft_root/lib/mysql/* $soft_root/lib/
	ln -s $soft_root/include/mysql/* $soft_root/include/
fi

if [ `uname` = 'Linux' ]; then
	if [ "$mysql_ver" != '51' ]; then
		export LD_LIBRARY_PATH="${ld_lib_path}"
		if [ ! `grep -l "$soft_root/lib"    '/etc/ld.so.conf'` ]; then
			echo "$soft_root/lib" >> /etc/ld.so.conf
		fi
	else
		export LD_LIBRARY_PATH="${ld_lib_path}"
		if [ ! `grep -l "$soft_root/lib/mysql"    '/etc/ld.so.conf'` ]; then
			echo "$soft_root/lib" >> /etc/ld.so.conf
		fi
	fi

	ldconfig
fi

ln -s ${soft_root}/bin/* $LuNamp_root/other_cmd/

#判断升级是否成功
if [ "$2" = '-y' ] && [ "$1" != '' ]; then
	if [ "`${soft_root}/bin/mysql -V | grep -c '$1'`" ]; then
		showmsg "MySQL数据库升级成功" "The mysql was updated successfully!" $color_green $color_white
	else
		showmsg "MySQL数据库升级失败" "The mysql was field updated." $color_red $color_white
	fi
	exit;
fi

cd ${i_soft_root}
rm -rf mysql-${soft_version}
cd ${i_code_root}


