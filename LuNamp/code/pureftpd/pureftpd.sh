#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

if [ -e "./common/common.sh" ]; then
	. "./common/common.sh"
else
	. "../common/common.sh"
fi

soft_root="/usr/local/pureftpd"
soft_version="1.0.36"
if [ "$1" != '' ]; then
	soft_version=$1;
fi

cd ${i_soft_root}

tar -zxvf pure-ftpd-${soft_version}.tar.gz
cd pure-ftpd-${soft_version}

if [ `uname` = 'Linux' ]; then
	export LD_LIBRARY_PATH="${ld_lib_path}"
	ldconfig
fi

#./configure --prefix=$soft_root CFLAGS=-O2 \
./configure --prefix=$soft_root \
--with-pgsql=/usr/local/pgsql \
--with-mysql=/usr/local/mysql \
--with-quotas \
--with-cookie \
--with-virtualhosts \
--with-virtualroot \
--with-diraliases \
--with-sysquotas \
--with-ratios \
--with-ftpwho \
--with-altlog \
--with-paranoidmsg \
--with-shadow \
--with-welcomemsg  \
--with-throttling \
--with-uploadscript

#--with-language=simplified-chinese \

make
make install

if [ ! -d '/home' ]; then
	mkdir /usr/home
	ln -s /usr/home /home
fi
mkdir -p /home/ftp
chmod 771 /home/ftp
chown ftp:LuManager /home/ftp

cp configuration-file/pure-config.pl $soft_root/sbin/
chmod 700 $soft_root/sbin/pure-config.pl

cp ${i_code_root}/pureftpd/pureftpd-mysql.conf $soft_root/
cp ${i_code_root}/pureftpd/pureftpd-pgsql.conf $soft_root/

cp ${i_code_root}/pureftpd/pure-ftpd.conf $soft_root/

chmod 4711 $soft_root/sbin/pure-ftpwho

ln -s ${soft_root}/bin/* $LuNamp_root/other_cmd/
ln -s ${soft_root}/sbin/* $LuNamp_root/other_cmd/

cd ${i_soft_root}
rm -rf pure-ftpd-${soft_version}
cd ${i_code_root}
