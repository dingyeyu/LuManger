#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

if [ -e "./common/common.sh" ]; then
	. "./common/common.sh"
else
	. "../common/common.sh"
fi


soft_root="/usr/local/nginx"
soft_version="1.3.0"
if [ "$1" != '' ]; then
	soft_version=$1;
fi

cd ${i_soft_root}

openssl_version="1.0.1c"
rm -rf openssl-${openssl_version}
tar -zxvf openssl-${openssl_version}.tar.gz

if [ -d "tengine" ]; then
	rm -rf tengine
else
	rm -rf tengine-${soft_version}
fi
tar -zxvf tengine-${soft_version}.tar.gz
if [ -d "tengine" ]; then
	cd tengine
else
	cd tengine-${soft_version}
fi

if [ `uname` = 'Linux' ]; then
	extends=''
else
	extends=''
fi

lun_debug_func "start"

./configure --prefix=${soft_root} ${extends} \
--with-pcre=../pcre-8.10 \
--with-openssl=../openssl-1.0.1c \
--add-module=../ngx_cache_purge-1.3 \
--with-http_stub_status_module \
--with-http_concat_module \
--with-http_ssl_module \
--with-http_gzip_static_module \

#--with-zlib=/usr/local/${ext_soft_dir}zlib

#--with-http_perl_module \
#--with-http_flv_module \
#--with-perl=/usr/local/perl \
#--without-http-cache \

make
make install

lun_debug_func "end"

cat ${i_code_root}/nginx/enable_php.conf > $soft_root/conf/enable_php.conf

cat ${i_code_root}/tengine/nginx.conf > $soft_root/conf/nginx.conf
chown LuManager $soft_root/conf/nginx.conf

cat ${i_code_root}/nginx/proxy_pass.conf > $soft_root/conf/proxy_pass.conf

cat ${i_code_root}/nginx/vhost_default.conf > $soft_root/conf/vhost.conf;

ln -s ${soft_root}/bin/* $LuNamp_root/other_cmd/

if [ -d '/usr/local/nginx' ]; then
	rm -rf /usr/local/tengine.bak
	cp -a /usr/local/nginx /usr/local/tengine.bak
fi

cd ${i_soft_root}
rm -rf tengine
cd ${i_code_root}

