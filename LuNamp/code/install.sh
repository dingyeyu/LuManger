#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

if [ -e "./common/common.sh" ]; then
	. "./common/common.sh"
else
	. "../common/common.sh"
fi

# LuNamp install Start
if [ "${is_Linux}" != 'Yes' ] && [ "${is_FreeBSD}" != 'Yes' ]; then
	showmsg "您的系统不能安装LuNamp，谢谢您的关注！"    "Sorry! This system can't install LuNamp."	$color_red	$color_white
	exit;
fi

lun_debug_func "end"

if [ "${is_Linux}" = 'Yes' ]; then
	if [ "${linux_type}" = 'Debian' ]; then
		if [ "$1" != '-y' ]; then
			showmsg "您装的是Linux系统，请确保服务器能上网才能安装${LuNamp_ver}所需的软件包，现在下载并安装软件吗？[Y/n]"  "If you want to install ${LuNamp_ver}, You must to install some environment softwares. Do you wish to continue? [Y/n]"
			read install_env_soft;
		fi

		if [ "$1" != '-y' ]; then
			showmsg "需要升级您的Linux系统吗？[Y/n]"  "Do you want to update your Linux system? [Y/n]"
			read update_system;
		else
			update_system='y'
		fi
		if [ "$update_system" != 'n' ] && [ "$update_system" != 'N' ]; then
			showmsg "开始升级系统..."	"Start to update your system..."
			sleep 1;
			apt-get -y update;
			#apt-get -y upgrade;
		fi

		if [ "$1" = '-y' ] || [ "$install_env_soft" = 'Y' ] || [ "$install_env_soft" = 'y' ] || [ "$install_env_soft" = '' ]; then
			showmsg "开始下载并安装软件..."		"Start to download and install environment softwares..."
			
			echo $sleep_time;

			softs='gcc g++ ssh make bzip2 flex vim bison libtool libncurses5-dev libncurses5 libncurses5-dev libncurses5-dev libpcrecpp0 patch ntpdate openssl libssl-dev build-essential file gawk binutils parted zip unzip libperl-dev perl ftp libreadline-dev libreadline5-dev libapr1-dev libxml2-dev libxml2';
			if [ "${linux_version}" != 'Debian' ]; then
				softs="${softs} libc6-dev cpp autoconf automake libtool build-essential";
			fi
			apt-get -y install $softs
			for soft_name in $softs; do
				if [ "`dpkg -l | grep ii | grep -c ${soft_name}`" -eq "0" ]; then
					if [ "$1" = '-y' ] || [ "$1" = '-Y' ]; then
						apt-get -y --force-yes install $soft_name;
					else
						apt-get install $soft_name;
					fi
				fi
			done;
			apt-get -y install zip bzip2;
			echo $enter4
		else
			showmsg "您必须先编译LuNamp所需的安装环境软件！" 'You must to install environment softwares!' ${color_red} ${color_white}
		fi
		
		if [ ! "`dpkg -l | grep ii | grep g++`" ] || [ ! "`dpkg -l | grep ii | grep bzip2`" ] || [ ! "`dpkg -l | grep ii | grep patch`" ]; then
			showmsg "环境软件安装失败，退出LuNamp的安装（这不是LuNamp的bug，有可能是网络或者系统有问题，请确保可以使用apt-get安装软件）"	"Environment softwares was install failed, quit to install LuNamp."
			showmsg "需要强制安装LuNamp吗？[y/N]"	"Do you want to continue for install LuNamp?[y/N]"
			read lun_install_continue
			if [ "$lun_install_continue" != 'y' ] && [ "$lun_install_continue" != 'Y' ]; then
				exit;
			fi
		fi
		
	elif [ "${linux_type}" = 'RedHat' ]; then
		if [ "$1" = '-y' ] || [ ! "`yum list installed |grep gcc-c++`" ] || [ ! "`yum list installed |grep ftp`" ] || [ ! "`yum list installed |grep parted`" ] || [ ! "`yum list installed |grep openssl-devel`" ] || [ ! "`yum list installed |grep libtool-libs`" ] || [ ! "`yum list installed |grep ncurses-devel`" ] || [ ! "`yum list installed |grep patch`" ] || [ ! "`yum list installed |grep libxml2-devel`" ] || [ ! "`yum list installed |grep ntp`" ] || [ ! "`yum list installed |grep zip`" ] || [ ! "`yum list installed |grep perl`" ] || [ ! "`yum list installed |grep curl-devel`" ]; then
			if [ "$1" != '-y' ]; then
				showmsg "您装的是Linux系统，请确保服务器能上网才能安装${LuNamp_ver}所需的软件包，现在下载并安装软件吗？[Y/n]"		"If you want to install ${LuNamp_ver}, You must to install some environment softwares. Do you wish to continue? [Y/n]"	${color_green}	${color_white}
				read install_env_soft;
			fi

			if [ "$1" != '-y' ]; then
				showmsg "需要升级您的Linux系统吗？[Y/n]"  "Do you want to update your Linux system? [Y/n]"
				read update_system;
			else
				update_system='n'
			fi
			if [ "$update_system" != 'n' ] && [ "$update_system" != 'N' ]; then
				showmsg "开始升级系统..."	"Start to update your system..."
				sleep 1;
				yum -y update;
				#yum $1 upgrade;
			fi

			if [ "$1" = '-y' ] || [ "$install_env_soft" = 'Y' ] || [ "$install_env_soft" = 'y' ] || [ "$install_env_soft" = '' ]; then
				showmsg	"开始下载并安装软件..."		"Start to download and install environment softwares..."
				sleep $sleep_time;
				#yum -y install yum-utils
				#yum-complete-transaction --cleanup-only
				#package-cleanup --dupes
				#package-cleanup --problems

				softs='gcc gcc-c++ bzip2 make vixie-cron flex libevent ncurses-devel wget patch ntp m4 autoconf zip unzip openssl openssl-devel file libtool libtool-libs gmp-devel pspell-devel parted zlib perl mod_perl-devel apr-util ftp readline-devel readline-devel apr apr-util apr-devel openssh-clients';
				if [ "$lun_debug" != "1" ]; then
					softs="${softs} libjpeg libjpeg-devel gd gd-devel freetype freetype-devel libpng libpng-devel";	
					softs="${softs} libxml2 libxml2-devel curl curl-devel";
				fi
				yum -y install $softs
				#for soft_name in $softs; do
				#	if [ "`yum list installed |grep -c ${soft_name}`" -eq "0" ]; then
				#		yum $1 reinstall $soft_name;
				#	fi
				#done;

				for soft_name in $softs; do
					if [ "`yum list installed |grep -c ${soft_name}`" -eq "0" ]; then
						yum $1 --skip-broken install $soft_name;
					fi
				done;
				yum -y --skip-broken install zip bzip2;
				#yum $1 install gcc gcc-c++ bzip2 make vixie-cron flex ncurses-devel wget patch ntp libxml2 libxml2-devel libevent m4 autoconf zip unzip libjpeg libjpeg-devel gd gd-devel freetype freetype-devel libpng libpng-devel openssl openssl-devel file libtool libtool-libs kernel-devel gmp-devel pspell-devel parted zlib perl apr-util ftp readline-devel
				echo $enter4
			else
				showmsg "您必须先编译LuNamp所需的安装环境软件！" 'You must to install environment softwares!' ${color_red} ${color_white}
				exit;
			fi
		fi

		if [ ! "`yum list installed |grep gcc-c++`" ] || [ ! "`yum list installed |grep openssl-devel`" ] || [ ! "`yum list installed |grep ncurses-devel`" ] || [ ! "`yum list installed |grep patch`" ]; then
			showmsg "环境软件安装失败，退出LuNamp的安装（这不是LuNamp的bug，有可能是网络或者系统有问题，请确保可以使用yum安装软件）"	"Environment softwares was install failed, quit to install LuNamp."
			showmsg "需要强制安装LuNamp吗？[y/N]"	"Do you want to continue for install LuNamp?[y/N]"
			read lun_install_continue
			if [ "$lun_install_continue" != 'y' ] && [ "$lun_install_continue" != 'Y' ]; then
				exit;
			fi
		fi
		
		yum -y remove networkManager
		#yum -y remove httpd
		#yum -y remove php
		#yum -y remove mysql-server mysql php-mysql
		#yum -y remove vsftpd
	else
		showmsg "非常抱歉！${LuNamp_ver}暂时还不能支持您的系统！请换成Zijidelu/FreeBSD/Debian/CentOS中的任何一种。"	"Sorry, You can not install ${LuNamp_ver} to your system! You can choosing Zijidelu, FreeBSD, Debian or CentOS."	$color_red	$color_white
		exit;
	fi
fi

echo 'Please wait to lock in time......'
if [ -f "/usr/bin/ntpdate" ] || [ -f "/usr/sbin/ntpdate" ] || [ -f "/usr/local/bin/ntpdate" ]; then
	ntpdate time.windows.com
fi
runtime 'start';

datetime=`date +"%Y%m%d"`
release_date=$(($release_date-1));#wrong date time, But can be used.
if test $datetime -lt $release_date; then
        #showmsg "您的系统时钟错误（${datetime}），请用date命令调整时间"    "Your system clock is wrong(${datetime}), Please use date command change it!"
	#showmsg "FreeBSD like this: date 201012050000(YmdHi), Linux like this: date 12050000(mdHiY)"
	#exit;

	if [ `uname` = 'FreeBSD' ]; then
		date ${release_y}${release_md}2359
	else
		date ${release_md}2359${release_y}
	fi
fi

chmod 755 ./*

if [ ! -d '/home' ]; then
	if [ ! -d /usr/home ]; then
		mkdir /usr/home
	fi
	ln -s /usr/home /home
fi

chmod -R 755 ${i_code_root}

user_and_group_add 2>&1 > /dev/null

echo
echo
echo "Starting to install ${LuNamp_var}, Please wait..."
echo
echo

sleep $sleep_time

#卸载旧软件
if [ -d "/usr/local/nginx" ] || [ -d "/usr/local/mysql" ] || [ -d "/usr/local/php" ] || [ -d "/usr/local/apache" ] || [ -d "/usr/local/pureftpd" ] || [ -d "/usr/local/zend" ]; then
	if [ ! -d "/usr/local/apache_LuManager" ] && [ ! -d "/usr/local/LuNamp" ]; then
		showmsg "发现一些与LuNamp有冲突的软件，要删除后再装LuNamp吗？[y/N]"   "If you want to continue, you must to delete other softwares, do you want to continue?[y/N]"
		read uninstall_old_soft
		
		if [ "$uninstall_old_soft" = 'y' ] ||  [ "$uninstall_old_soft" = 'Y' ]; then
			${i_code_root}/uninstall.sh
		fi
	fi
fi

rm -f /home/ftp/LuNamp/index.php;
install_func "LuNamp"    "/home/ftp/LuNamp/index.php"    "./LuNamp/install.sh"

if [ ! -e "$LuManager_root/index.php" ]; then
	kill_soft httpd 2>&1 > /dev/null
	kill_soft apache2 2>&1 > /dev/null
	kill_soft apache 2>&1 > /dev/null
	kill_soft lighttpd  2>&1 > /dev/null
	kill_soft mysqld 2>&1 > /dev/null
	kill_soft mysql 2>&1 > /dev/null
	kill_soft ftpd 2>&1 > /dev/null
	kill_soft vsftpd 2>&1 > /dev/null
	kill_soft proftpd 2>&1 > /dev/null
	kill_soft pure-ftpd 2>&1 > /dev/null
	kill_soft nginx 2>&1 > /dev/null
	kill_soft xinetd 2>&1 > /dev/null
fi

if [ -d "/usr/lib64" ]; then
	ln -s /usr/lib64/* /usr/lib/ 2>&1 > /dev/null
fi

if [ `uname` = 'Linux' ]; then
	export LD_LIBRARY_PATH="${ld_lib_path}"

	if [ ! `grep -l '/usr/local/lib'    '/etc/ld.so.conf'` ]; then
		echo "/usr/local/lib" >> /etc/ld.so.conf
	fi

	if [ -d "/usr/lib64" ] && [ ! `grep -l '/usr/lib64'    '/etc/ld.so.conf'` ]; then
		echo "/usr/lib64" >> /etc/ld.so.conf
	fi

	if [ ! `grep -l '/usr/lib'    '/etc/ld.so.conf'` ]; then
		echo "/usr/lib" >> /etc/ld.so.conf
	fi

	if [ ! `grep -l "/lib"    '/etc/ld.so.conf'` ]; then
		echo "/lib" >> /etc/ld.so.conf
	fi

	ldconfig
else
	limits -v unlimited
fi

install_func "Zlib"    "/usr/local/${ext_soft_dir}zlib"    "./common/zlib.sh"
ldconfig

install_func "Libxml2"    "/usr/local/${ext_soft_dir}libxml"    "./php/libxml.sh"

install_func "Make"    "/usr/local/${ext_soft_dir}make"    "./common/make.sh"

if [ `uname` = 'FreeBSD' ]; then
	install_func "Zip"    "/usr/local/bin/zip"    "./common/zip.sh"
	
	install_func "UnZip"    "/usr/local/bin/unzip"    "./common/unzip.sh"

	install_func "Wget"    "/usr/local/wget/bin/wget"    "./common/wget.sh"
fi

#ai_ls=`ls -l ${i_soft_root}/mysql-5.*|grep '^-.*[5-9]\.[5-9]\|[6-9]\.[0-9]'`
#if [ ! "$ai_ls" ]; then
#	ai_ls=`ls -l ${i_soft_root}/mysql-6.*|grep '^-.*'`
#fi
#if [ ! "$ai_ls" ]; then
#	ai_ls=`ls -l ${i_soft_root}/mysql-7.*|grep '^-.*'`
#fi
#if [ ! "$ai_ls" ]; then
#	ai_ls=`ls -l ${i_soft_root}/mysql-8.*|grep '^-.*'`
#fi
#if [ "$ai_ls" ] && [ ! -d "/usr/local/mysql/bin" ]; then
if [ ! "`echo $mysql_ver|grep '[4-5].1.[0-9]'`" ] && [ ! -d "/usr/local/mysql/bin" ]; then
	install_func "CMake"    "/usr/local/${ext_soft_dir}cmake"    "./common/cmake.sh"
fi


if [ "${linux_type}" != 'RedHat' ] || [ "$lun_debug" = "1" ]; then
	install_func "Perl"    "/usr/local/${ext_soft_dir}perl"    "./common/perl.sh"
	install_func "Jpeg"    "/usr/local/${ext_soft_dir}jpeg"    "./php/jpeg.sh"
	install_func "Libpng"    "/usr/local/${ext_soft_dir}libpng"    "./php/libpng.sh"
else
	install_func "Perl"    "/usr/bin/perl -a ! -f /usr/sbin/perl"    "./common/perl.sh"	
fi

if [ `uname` = "Linux" ]; then
	ldconfig
fi

if [ -d "/usr/lib64" ]; then
	ln -s /usr/lib64/* /usr/lib/ 2>&1 > /dev/null
fi

if [ -d "/usr/local/lib64" ]; then
	ln -s /usr/local/lib64/* /usr/local/lib/ 2>&1 > /dev/null
fi

if [ `uname` = "Linux" ]; then
	ldconfig
fi

if [ "${linux_type}" != 'RedHat' ] || [ "$lun_debug" = "1" ]; then
	install_func "Freetype"    "/usr/local/${ext_soft_dir}freetype"    "./php/freetype.sh"
	install_func "GD"    "/usr/local/${ext_soft_dir}gd"    "./php/gd.sh"
fi

install_func "GDBM"    "/usr/local/${ext_soft_dir}gdbm"    "./php/gdbm.sh"

install_func "Iconv"    "/usr/local/${ext_soft_dir}libiconv/bin/iconv"    "./php/libiconv.sh" #gd b

install_func "Libevent"    "/usr/local/${ext_soft_dir}libevent/lib/libevent.so"    "./php/libevent.sh"

install_func "Memcached"    "/usr/local/memcached"    "./php/memcached.sh"

install_func "Libmcrypt"    "/usr/local/${ext_soft_dir}libmcrypt"    "./php/libmcrypt.sh"

install_func "Curl"    "/usr/local/curl"    "./php/curl.sh"

if [ "${linux_type}" != 'RedHat' ] || [ "$lun_debug" = "1" ]; then
	install_func "M4"    "/usr/local/bin/m4"    "./common/m4.sh"
	install_func "Autoconf"    "/usr/local/bin/autoconf"    "./common/autoconf.sh"
fi

if [ -d "/usr/lib64" ]; then
	ln -s /usr/lib64/* /usr/lib/ 2>&1 > /dev/null
fi

# ----------------- pgsql
install_func "PgSQL"    "/usr/local/pgsql/bin/createdb"    "./pgsql/pgsql.sh"
$LuNamp_root/cmd/pgsql-stop

# ----------------- mysql
install_func "Mysql"    "/usr/local/mysql/bin/mysql -o ! -d ${mysql_data_path}/mysql"    "./mysql/mysql.sh"

# ----------------- mysql_data
if [ ! `ps -Af | grep 'mysqld_safe' | grep -vc 'grep'` != '0' ]; then
	$LuNamp_root/cmd/mysql-start
fi
sleep 10
if [ ! `ps -Af | grep 'mysqld_safe' | grep -vc 'grep'` != '0' ]; then
	showmsg "MySQL启动失败！"    "MySQL was started failed."    $color_green    $color_white
	exit;
fi


if [ ! -e "${mysql_data_path}/LuManager" ]; then
	/usr/local/mysql/bin/mysql --default-character-set=utf8 < ./mysql/mysql_data.sql.conf
fi
if [ ! -e "${mysql_data_path}/LuManager" ]; then
	showmsg "MySQL数据导入不成功，可能是数据库没启动，或者是MySQL没安装成功，请重装一次试试！"    "Mysql data was not imported！"		$color_red	$color_red
	exit;
else
	echo -e $enter4
	showmsg "MySQL数据导入成功！"    "Mysql data was imported!"		$color_green	$color_white
fi
sleep $sleep_time

# ----------------- pgsql
install_func "Openssl"    "/usr/local/${ext_soft_dir}openssl"    "./common/openssl.sh"

# ----------------- nginx
install_func "Php_fcgi"    "/usr/local/php_fcgi/etc/php-fpm.conf"    "./php/php_fcgi.sh"
if [ ! -d "/usr/local/tengine.bak" ]; then
	rm -rf /usr/local/nginx
	install_func "Tengine"    "/usr/local/nginx"    "./tengine/tengine.sh"
	if [ ! -d "/usr/local/tengine.bak" ]; then
		cp -R /usr/local/nginx /usr/local/tengine.bak
	fi
fi
if [ ! -d "/usr/local/nginx.bak" ]; then
	rm -rf /usr/local/nginx;
	install_func "Nginx"    "/usr/local/nginx"    "./nginx/nginx.sh"
	if [ ! -d "/usr/local/nginx.bak" ]; then
		cp -R /usr/local/nginx /usr/local/nginx.bak
	fi
fi
if [ ! -d "/usr/local/nginx" ]; then
	cp -R /usr/local/nginx.bak /usr/local/nginx
fi

php_path="/usr/local/php_fcgi";
ext_path=`find $php_path/lib/php/extensions/ -name "*-*-*-*" | grep "^$php_path" | awk -F / '{print substr($0, match($0, "extensions/")+11)}'`
install_func "Eaccelerator_nginx"    "$php_path/lib/php/extensions/$ext_path/eaccelerator.so"    "./php/eaccelerator_nginx.sh"
install_func "Memcache_nginx"    "$php_path/lib/php/extensions/$ext_path/memcache.so"    "./php/memcache_nginx.sh"

install_func "spawn-fcgi"    "/usr/local/spawn-fcgi/"    "./php/spawn-fcgi.sh"

# ----------------- apache
if [ "$enable_apache" = "yes" ]; then
	install_func "Apache"    "/usr/local/apache/bin"    "./apache/apache.sh"
	install_func "Php"    "/usr/local/php/lib"    "./php/php.sh"
	
	php_path="/usr/local/php";
	ext_path=`find $php_path/lib/php/extensions/ -name "*-*-*-*" | grep "^$php_path" | awk -F / '{print substr($0, match($0, "extensions/")+11)}'`
	install_func "Eaccelerator_apache"    "$php_path/lib/php/extensions/$ext_path/eaccelerator.so"    "./php/eaccelerator_apache.sh"
	install_func "Memcache_apache"    "$php_path/lib/php/extensions/$ext_path/memcache.so"    "./php/memcache_apache.sh"
fi

# ----------------- apache_LuManager
install_func "Apache_LuManager"    "/usr/local/apache_LuManager/bin"    "./apache/apache_LuManager.sh"
if [ ! -d "/usr/local/apache_LuManager.bak" ]; then
	cp -a /usr/local/apache_LuManager /usr/local/apache_LuManager.bak
fi

# ----------------- php_LuManager
install_func "Php_LuManager"    "/usr/local/php_LuManager/bin"    "./php/php_LuManager.sh"
if [ ! -d "/usr/local/php_LuManager.bak" ]; then
	cp -a /usr/local/php_LuManager /usr/local/php_LuManager.bak
fi
#php_path="/usr/local/php_LuManager";
#ext_path=`find $php_path/lib/php/extensions/ -name "*-*-*-*" | grep "^$php_path" | awk -F / '{print substr($0, match($0, "extensions/")+11)}'`;
#echo
#install_func "Eaccelerator_LUM"    "$php_path/lib/php/extensions/${ext_path}/eaccelerator.so"    "./php/eaccelerator_apache_LuManager.sh";
#install_func "Memcache_LUM"    "$php_path/lib/php/extensions/${ext_path}/memcache.so"    "./php/memcache_apache_LuManager.sh";

lun_debug_func "end"

# ----------------- pureftpd
install_func "Pure-ftpd"    "/usr/local/pureftpd"    "./pureftpd/pureftpd.sh"

# ----------------- rsync
install_func "rsync"    "/usr/local/rsync"    "./common/rsync.sh"

# ----------------- bind
if [ "$enable_bind" = 'yes' ]; then
	install_func "Bind"    "/usr/local/bind"    "./bind/bind.sh"
fi

# ----------------- zend
install_func "Zend"    "/usr/local/Zend/lib"    "./php/zend.sh"

if [ `uname` = 'Linux' ]; then
	install_func "CpuLimit"    "$LuNamp_root/other_cmd/cpulimit"    "./common/cpulimit.sh"
fi

install_func "LuManager"    "$LuManager_root/index.php"    "./LuManager/LuManager.sh"

# ----------------- phpMyAdmin
install_func "PhpMyAdmin"    "$LuNamp_root/pm"    "./mysql/phpMyAdmin.sh"

# ----------------- phpPgAdmin
install_func "PhpPgAdmin"    "$LuNamp_root/pp"    "./pgsql/phpPgAdmin.sh"


if [ `uname` = 'FreeBSD' ] && [ ! `grep -l "LuNamp ipfw start"    "/etc/rc.conf"` ]; then
	./firewall/install.sh
elif [ -e '/etc/sysconfig/iptables' ]; then
	cat './firewall/iptables.conf' > '/etc/sysconfig/iptables'
	/etc/init.d/iptables stop
fi

#ln -sf $LuNamp_root/cmd /root/lu_cmd

#----其它软件的命令链接
ln -s /usr/local/apache/bin/* $LuNamp_root/other_cmd/ 2>&1 > /dev/null
ln -s /usr/local/bind/bin/* $LuNamp_root/other_cmd/ 2>&1 > /dev/null
ln -s /usr/local/curl/bin/* $LuNamp_root/other_cmd/ 2>&1 > /dev/null
ln -s /usr/local/${ext_soft_dir}libiconv/bin/* $LuNamp_root/other_cmd/ 2>&1 > /dev/null
ln -s /usr/local/memcached/bin/* $LuNamp_root/other_cmd/ 2>&1 > /dev/null
ln -s /usr/local/mysql/bin/* $LuNamp_root/other_cmd/ 2>&1 > /dev/null
ln -s /usr/local/pgsql/bin/* $LuNamp_root/other_cmd/ 2>&1 > /dev/null
ln -s /usr/local/nginx/sbin/* $LuNamp_root/other_cmd/ 2>&1 > /dev/null
ln -s /usr/local/pureftpd/bin/* $LuNamp_root/other_cmd/ 2>&1 > /dev/null
ln -s /usr/local/pureftpd/sbin/* $LuNamp_root/other_cmd/ 2>&1 > /dev/null
ln -s /usr/local/rsync/bin/* $LuNamp_root/other_cmd/ 2>&1 > /dev/null
ln -s /usr/local/${ext_soft_dir}openssl/bin/* $LuNamp_root/other_cmd/ 2>&1 > /dev/null
if [ -f "/usr/local/wget/bin/wget" ]; then
	ln -s /usr/local/wget/bin/* $LuNamp_root/other_cmd/ 2>&1 > /dev/null
fi


export PATH="$PATH:/usr/local/LuNamp/cmd/:/usr/local/LuNamp/other_cmd/";

if [ `uname` = 'Linux' ]; then
	if [ ! `grep -l "$LuNamp_root/cmd"    "/etc/profile"` ]; then
		echo "export PATH=\"$PATH:$LuNamp_root/cmd/:$LuNamp_root/other_cmd/\"" >> "/etc/profile";
	fi

	#if [ ! `grep -l "$LuNamp_root/other_cmd"    "/etc/profile"` ]; then
	#	echo "export PATH=\"$PATH:$LuNamp_root/other_cmd/\"" >> "/etc/profile";
	#fi

	if [ ! `grep -l "pgsql"    "/etc/profile"` ]; then
		echo 'export LD_LIBRARY_PATH="/usr/local/mysql/lib:/usr/local/mysql/lib/mysql:/usr/local/pgsql/lib:/usr/lib:/lib:/usr/local/lib"' >> "/etc/profile";

		if [ `grep -l 'SELINUX=enforcing'    '/etc/sysconfig/selinux'` ]; then
			setenforce 0
			sed -i 's/SELINUX=enforcing/SELINUX=disabled/g'    '/etc/sysconfig/selinux'
		fi
	
		if [ ! `grep -l "/usr/local/lib"    "/etc/ld.so.conf"` ]; then
			echo "/usr/local/lib" >> "/etc/ld.so.conf"
			ldconfig
		fi
	fi

	. "/etc/profile";
else
	#set path = ($path ${LuNamp_root}/cmd);
	if [ ! `grep -l "$LuNamp_root/cmd"    "/root/.cshrc"` ]; then
		echo "set path = (\$path $LuNamp_root/cmd $LuNamp_root/other_cmd)" >> "/root/.cshrc";
	fi
	#if [ ! `grep -l "$LuNamp_root/other_cmd"    "/root/.cshrc"` ]; then
	#	echo "set path = (\$path $LuNamp_root/other_cmd)" >> "/root/.cshrc";
	#fi
fi


start_script_file="/etc/rc.local"
if [ `grep -l "exit 0"    "$start_script_file"` ]; then
	str_replace    "exit 0/#exit 0"    "$start_script_file";
fi

if [ ! `grep -l "lu-start"    "$start_script_file"` ]; then
	cat ${i_code_root}/LuNamp/rc_local_add.sh >> $start_script_file
fi
if [ ! `grep -l "#LuNamp OPT start"    "/etc/sysctl.conf"` ] && [ -e "${i_code_root}/opt/sysctl_${uname}.conf" ]; then
	cat "${i_code_root}/opt/sysctl_${uname}.conf" >> "/etc/sysctl.conf"
fi
if [ ! `grep -l "#LuNamp OPT start"    "/etc/security/limits.conf"` ] && [ -e "${i_code_root}/opt/limits_${uname}.conf" ]; then
	cat "${i_code_root}/opt/limits_${uname}.conf" >> "/etc/security/limits.conf"
fi

$LuNamp_root/cmd/lu-start

${i_code_root}/common/copyright.sh

lun_debug_func "end"

if [ -e "$LuManager_root/index.php" ]; then
	rm -rf /tmp/LuNamp_install_status.txt

	showmsg "LuNamp安装完成，谢谢您的使用！"    "LuNamp was installed successfully!, thank you for choosing LuNamp! "    $color_green    $color_white
	runtime 'end'
	
	showmsg "1: 安装LuManager. 2: 升级LuManager. 0: 退出"    "1: Install LuManager. 2: Update LuManager. 0: Exit. "    $color_green    $color_white
	read starting_install_LuManager
	if [ "$starting_install_LuManager" = '1' ] && [ ! -f "/var/tmp/LuNamp_install_from_script.lock" ]; then
		if [ -f "${i_code_root}/../../i.sh" ]; then
			cd ${i_code_root}/../../
			./i.sh -y
		else
			cd ${i_code_root}/../../
			${my_wget} http://down.zijidelu.org/zijidelu_install.sh
			chmod 744 zijidelu_install.sh
			./zijidelu_install.sh -y
		fi
	elif [ "$starting_install_LuManager" = '2' ] && [ ! -f "/var/tmp/LuNamp_install_from_script.lock" ]; then
		cd ${i_code_root}/../../
		${my_wget} http://down.zijidelu.org/zijidelu_update.sh
		chmod 744 zijidelu_update.sh
		./zijidelu_update.sh -y
	else
		if [ -f "/var/tmp/LuNamp_install_from_script.lock" ]; then
			rm -f "/var/tmp/LuNamp_install_from_script.lock";
		fi

		showmsg '请使用http://IP:8888或http://domain:8888访问后台管理系统（IP就是您的服务器IP，domain即是指向该IP的域名）。' 'Please use "http://your ip:8888" or "http://your domain:8888" and login LuNamp server to manager system!'
		if [ ! -e '../../i.sh' ]; then
			showmsg '为保证所有程序都能正常使用，需重启系统，立即重启吗？[Y/n]'        'In order to LuManager normal operation, You must reboot the system, reboot now?[Y/n]'
			read reboot_now
			if [ "$reboot_now" != 'n' ] && [ "$reboot_now" != 'N' ]; then
				reboot
				exit;
			fi
		fi
	fi

else 
	echo -e $enter8
	showmsg 'LuNamp安装失败！' 'Sorry! LuNamp is not installed.'	 $color_red	$color_white
	runtime 'end'
fi

cd ~

