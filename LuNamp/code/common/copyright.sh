#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

if [ -e "./common/common.sh" ]; then
	. "./common/common.sh"
else
	. "../common/common.sh"
fi


echo $color_cyan
echo
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "+"
echo "+ Software:  $LuNamp_ver"
echo "+ Author:    Liu Xin（网名：爱洞特漏）"
echo "+ Website:   www.zijidelu.org（自己的路）"
echo "+ Email:     service@zijidelu.org"
echo "+"
echo "+ Thank you for choosing $LuNamp_ver!"
echo "+"
echo "+ License:"
echo "+ 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html"
echo "+ Thank you for choosing LuNamp, This program is free software; "
echo "+ you can redistribute it and/or modify it under the terms of the GNU General "
echo "+ Public License as published by the Free Software Foundation; "
echo "+ either version 2 of the License, or (at your option) any later version."
echo "+ http://www.gnu.org/copyleft/gpl.html"
echo "+"
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo
echo $color_white

