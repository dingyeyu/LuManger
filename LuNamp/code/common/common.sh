#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

LuNamp_ver='LuNamp2.6'
release_y='2012'
release_md='0812'
release_date=${release_y}${release_md}

lun_debug='0'

LuNamp_root='/usr/local/LuNamp'
LuManager_root='/usr/local/LuManager'

ext_soft_dir='LuNamp_ext/';

if [ -f "`pwd`/code/common/common.sh" ]; then
	i_code_root="`pwd`/code"
elif [ -f "`pwd`/common/common.sh" ]; then
	i_code_root="`pwd`"
elif [ -f "`pwd`/../common/common.sh" ]; then
	i_code_root="`pwd`/.."
elif [ -f "`pwd`/../../common/common.sh" ]; then
	i_code_root="`pwd`/../.."
fi
i_soft_root="${i_code_root}/../soft"

if [ -f "${i_code_root}/../../_debug.sh" ]; then
	echo
	echo "Use debug mode";
	sleep 2;
	echo
	. "${i_code_root}/../../_debug.sh"
fi

######
if [ "`id -u`" != "0" ]; then
	echo "请用root用户安装LuNamp！"
	echo
	echo "Please use root user to install LuNamp!"
	echo
	exit;
fi

if [ ! -d "$LuNamp_root/cmd" ]; then
	mkdir -p "$LuNamp_root/cmd";
	chmod -R 554 $LuNamp_root
fi

if [ ! -d "$LuNamp_root/other_cmd" ]; then
	mkdir -p "$LuNamp_root/other_cmd";
	chmod -R 554 $LuNamp_root
fi

if [ ! -d "$LuManager_root" ]; then
	mkdir -p "$LuManager_root";
fi

. "${i_code_root}/install_config.sh"
if [ -f "${i_code_root}/../../lun_install_config.sh" ]; then
	. "${i_code_root}/../../lun_install_config.sh"
fi

mysql_data_path="/home/mysql_data";#mysql数据库路径
pgsql_data_path="/home/pgsql_data";#pgsql数据库路径
if [ -f "/home/lum_safe_files/cmd/config.inc.sh" ]; then
	. "/home/lum_safe_files/cmd/config.inc.sh";
fi

color_cyan="[40;36m"
color_red="[40;31m"
color_yellow="[40;33m"
color_green="[40;32m"
color_white="[40;37m"

continue_install_string="was installed successfully! Continue..."
continue_install_string_cn="安装成功，继续安装$LuNamp_ver"
stop_install_string="is not installed! Stop to install $LuNamp_var."
stop_install_string_cn="安装不成功！停止安装$LuNamp_ver"

if [ ! "$ld_lib_path" ]; then
	ld_lib_path="/usr/local/mysql/lib:/usr/local/mysql/lib/mysql:/usr/local/pgsql/lib:/usr/lib:/usr/lib64:/lib:/lib64:/usr/local/lib"
fi

sleep_time=2;

enter2="\n\n";
enter4=${enter2}${enter2};
enter8=${enter4}${enter4};

system_hardware=`uname -m`;
uname=`uname`

server_name=`uname -n`;

if [ `uname` = 'FreeBSD' ]; then
	user_del='pw userdel';
	user_add='pw useradd';
	group_del='pw groupdel';
	group_add='pw groupadd';

	root_group='wheel'

	is_FreeBSD='Yes'

	if [ "$system_hardware" = 'amd64' ]; then
		system_bit='64'
	else
		system_bit='32'
	fi

	my_wget="fetch";

	system_version=`uname -r`
	system_ver=`echo $system_version|cut -c1-1`
else
	user_del='userdel';
	user_add='useradd';
	group_del='groupdel';
	group_add='groupadd';

	#if [ -d "/etc/sysconfig" ] && [ "`yum list installed |grep bash`" ]; then
	#	linux_type='RedHat';
	#else
	#	linux_type='Debian';
	#fi

	if [ -d "/etc/apt" ] && [ -f "/usr/bin/dpkg" ]; then
		linux_type='Debian';
	else
		linux_type='RedHat';
	fi

	my_wget="wget";

	linux_version_1=`head -1 /etc/issue`;
	linux_version_2=`echo $linux_version_1|cut -c1-10`;

	if [ `echo $linux_version_2 | grep -c 'CentOS'` = "1" ]; then
		linux_version='CentOS';
	elif [ `echo $linux_version_2 | grep -c 'Zijidelu'` = "1" ]; then
	linux_version='Zijidelu';
	elif [ `echo $linux_version_2 | grep -c 'Red Hat'` = "1" -o `echo $linux_version_2 | grep -c 'RedHat'` = "1" ]; then
	linux_version='RedHat';
	elif [ `echo $linux_version_2 | grep -c 'Debian'` = "1" ]; then
		linux_version='Debian';
	elif [ `echo $linux_version_2 | grep -c 'Ubuntu'` = "1" ]; then
		linux_version='Ubuntu';
	fi

	root_group='root'
	is_Linux='Yes'

	if [ "$system_hardware" = 'x86_64' ]; then
		system_bit='64'
	else
		system_bit='32'
	fi

	ulimit -v unlimited
	ulimit -m unlimited
fi


showmsg () {
	NO3_var=$3
	NO4_var=$4

	if [ ! "$NO3_var" ]; then
		NO3_var=$color_yellow
	fi

	if [ ! "$NO4_var" ]; then
		NO4_var=$color_white
	fi

	echo
	echo $NO3_var$1$NO4_var
	if [ "$2" ]; then
		echo $NO3_var$2$NO4_var
	fi
	echo
}

runtime () {
	if [ "$1" = 'start' ]; then
		start_time=`date +"%s"`
		echo ${start_time} > "/var/tmp/LuNamp_runtime"
		sleep 2
	else
		start_time="`cat /var/tmp/LuNamp_runtime`"
		#if [ "$start_time" \> "0" ] && [ "$start_time" \< "99999999999" ]; then
		if [ ! "`echo $start_time|egrep '^[0-9]+$'`" ]; then
			echo;#单个安装
		else
			#正常安装
			end_time=`date +"%s"`
			total_s=$(($end_time - $start_time))
			total_m=$(($total_s / 60))

			if test $total_s -lt 60; then
				time_cn="${total_s} 秒钟"
				time_en="${total_s} Seconds"
			else
				time_cn="${total_m} 分钟"
				time_en="${total_m} Minutes"
			fi
			
			if [ "$1" = 'end' ]; then
				echo "Install LuNamp runtime: ${time_cn}(${time_en})" > /var/tmp/LuNamp_runtime
				showmsg "总耗时：${time_cn}"     "Total runtime: ${time_en}"	 $color_yellow	$color_white
			else
				showmsg "已耗时：${time_cn}"     "Runtime: ${time_en}"	 $color_yellow	$color_white
			fi
		fi
	fi
}

install_func () {
	if [ ! -e $2 ]; then
		$3
	fi
	if [ ! -e $2 ]; then
		echo -e "$1 $stop_install_string" > /tmp/LuNamp_install_status.txt
		showmsg "$1 $stop_install_string_cn"    "$1 $stop_install_string"	$color_red	$color_white
		exit
	else
		echo -e "$1 $continue_install_string" > /tmp/LuNamp_install_status.txt
		echo -e $enter2
		showmsg "$1 $continue_install_string_cn"    "$1 $continue_install_string"	$color_green	$color_white
		echo -e $enter2
	fi

	runtime
	sleep $sleep_time
}


rm_soft () {
	rm -rf $1
	showmsg "$2 删除成功！"    "$2 was deleted successfully!"	$color_green $color_white
	sleep 1
}

# 1. s/r
# 2. file
str_replace () {
	if [ `uname` = 'Linux' ]; then
		sed -i "s/$1/g" $2
	else
		sed "s/$1/g" $2 > ${2}.bak
		sed "s/------------search--------------/------------replace--------------/g" ${2}.bak > ${2}
		rm -f ${2}.bak
	fi
}

kill_soft () {
	if [ "`echo $1|egrep '^[0-9]+$'`" ]; then
		port=`netstat -anp|grep LISTEN|grep ":$1 "|awk '{print $7}'|awk -F '/' '{print $1}'`
		if [ "$port" = '' ]; then
			echo '0';
			exit;
		fi
		kill -9 ${port};
	fi
	progress_id=`pgrep "$1"`
	if [ "$progress_id" != '' ]; then
		for id in ${progress_id}; do
			kill -9 ${id};
		done
	fi
}

lun_debug_func () {
	if [ "$lun_debug" = '1' ]; then
		lun_debug1 "$1"
		return;
	fi

	if [ "`uname`" != 'Linux' ]; then
		return;
	fi

	setenforce 0
	ldconfig
	export LD_LIBRARY_PATH="${ld_lib_path}"
}

user_and_group_add () {
	if [ ! -d '/usr/local/LuNamp' ]; then
		$user_del 1500
		$user_del LuManager
		$group_del 1500
		$group_del LuManager

		$user_del 1502
		$user_del ftp
		$group_del 1502
		$group_del ftpusers

		$user_del 1504
		$user_del lu_mysql
		$group_del 1504
		$group_del lu_mysql

		$user_del 1505
		$user_del lu_bind
		$group_del 1505
		$group_del lu_bind

		$user_del 1506
		$user_del postgres
		$group_del 1506
		$group_del postgres

		$user_del 1520
		$user_del LuAdmin
		$group_del 1520
		$group_del LuGroup
	fi

	$group_add LuManager -g 1500
	$user_add LuManager -u 1500 -g LuManager -s /sbin/nologin -d /dev/null

	$group_add ftpusers -g 1502
	$user_add ftp -u 1502 -g ftpusers -s /sbin/nologin -d /dev/null

	$group_add lu_mysql -g 1504
	$user_add lu_mysql -u 1504 -g lu_mysql -s /sbin/nologin -d /dev/null

	$group_add lu_bind -g 1505
	$user_add lu_bind -u 1505 -g lu_bind -s /sbin/nologin -d /dev/null

	$group_add postgres -g 1506
	$user_add postgres -u 1506 -g postgres -d /dev/null

	$group_add zijideluGroup -g 1520
	$user_add zijidelu -u 1520 -g zijideluGroup -s /sbin/nologin -d /dev/null
	
	return;
}

###

