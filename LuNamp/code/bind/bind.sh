#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

if [ -e "./common/common.sh" ]; then
	. "./common/common.sh"
else
	. "../common/common.sh"
fi

soft_version="9.9.1-P1"
if [ "$1" != '' ]; then
	soft_version=$1;
fi
soft_root="/usr/local/bind"

cd ${i_soft_root}

rm -rf bind-${soft_version}
tar -zxvf bind-${soft_version}.tar.gz
cd bind-${soft_version}

if [ -d /usr/local/pgsql/bin ]; then
	ln -s /usr/local/pgsql/include/* /usr/local/include/
	ln -s /usr/local/pgsql/lib/* /usr/local/lib/
fi

extents=''
if [ "$bind_database_type" = 'pgsql' ]; then
	extents='--with-dlz-postgres=yes';
else
	extents='--enable-threads=no --with-dlz-mysql=/usr/local/mysql'
fi

./configure --prefix=${soft_root} ${extents} \
--with-openssl=/usr/local/${ext_soft_dir}openssl \
--with-libxml2=no \
--enable-largefile \
--enable-threads \
--with-dlz-filesystem

#--with-libxml2=/usr/local/${ext_soft_dir}libxml \
#--with-dlz-postgres=/usr/local/pgsql \
#--enable-threads=no \
#--with-dlz-mysql=/usr/local/mysql \

/usr/local/${ext_soft_dir}make/bin/make;
/usr/local/${ext_soft_dir}make/bin/make install clean;

ln -s ${soft_root}/bin/* $LuNamp_root/other_cmd/

cd ${i_soft_root}
rm -rf bind-${soft_version}
cd ${i_code_root}

