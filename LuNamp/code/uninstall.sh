#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

if [ -e "./common/common.sh" ]; then
	. "./common/common.sh"
else
	. "../common/common.sh"
fi


# LuNamp  delete start
showmsg "您已经备份好数据并确定要删除$LuNamp_var吗？ [y/N]"    "That you've already backuped your website and mysql data and delete $LuNamp_ver now?  [y/N]"
read answer

if [ "$answer" = "Y" ] || [ "$answer" = "y" ]; then
	$LuNamp_root/cmd/lu-stop
	
	rm_soft "/etc/my.cnf /usr/local/mysql"    "MySQL"
	if [ -d "${mysql_data_path}" ]; then
		mv ${mysql_data_path} ${mysql_data_path}.bak.`date +"%Y-%m-%d_%H:%M:%S"`
	fi

	rm_soft "/usr/local/pgsql"    "PostgreSQL"
	if [ -d "${pgsql_data_path}" ]; then
		mv ${pgsql_data_path} /home/pgsql_data.bak.`date +"%Y-%m-%d_%H:%M:%S"`
	fi

	rm_soft "/usr/local/apache"    "Apache"

	rm_soft "/usr/local/apache_LuManager"    "Apache_LuManager"

	rm_soft "/usr/local/nginx"    "Nginx"
	rm_soft "/usr/local/nginx.bak"    "Nginx_bak"
	rm_soft "/usr/local/tengine.bak"    "Tengine_bak"

	rm_soft "/usr/local/bind"    "Bind"

	rm_soft "/usr/local/pureftpd"    "Pureftpd"

	rm_soft "/usr/sbin/${ext_soft_dir}perl /usr/local/perl"    "Perl"

	rm_soft "/usr/local/curl"    "CURL"
	
	rm_soft "/usr/local/${ext_soft_dir}gd"    "GD"

	rm_soft "/usr/local/${ext_soft_dir}jpeg"    "Jpeg"

	rm_soft "/usr/local/${ext_soft_dir}libpng"    "Libpng"

	rm_soft "/usr/local/${ext_soft_dir}freetype"    "Freetype"

	rm_soft "/usr/local/${ext_soft_dir}make"    "Make"

	rm_soft "/usr/local/${ext_soft_dir}cmake"    "Cmake"

	rm_soft "/usr/local/${ext_soft_dir}libmcrypt"    "Libmcrypt"

	rm_soft "/usr/local/${ext_soft_dir}libiconv"    "Libiconv"

	rm_soft "/usr/local/php"    "Php"

	rm_soft "/usr/local/php_fcgi"    "Php_fcgi"

	rm_soft "/usr/local/php_LuManager"    "Php_LuManager"

	rm_soft "/usr/local/LuManager"    "LuManager"
	#rm -rf /root/lum_config.php

	rm_soft "/usr/local/rsync"    "Rsync"

	rm_soft "/usr/local/Zend"    "Zend"

	rm_soft "/usr/local/Zend_LuManager"    "Zend_LuManager"

	rm_soft "/usr/local/LuNamp"    "LuNamp_dir"
	#rm -rf /root/lu_cmd
 
	#echo '' > /etc/rc.local

	if [ `uname` = 'Linux' ]; then
		. '/etc/profile'
	fi
else
	showmsg	"您选择了取消删除 ${LuNamp_ver}。"    "You cancel to delete ${LuNamp_ver}."
fi

. "./common/copyright.sh"

