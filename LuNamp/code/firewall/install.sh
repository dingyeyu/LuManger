#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

if [ -e "./common/common.sh" ]; then
	. "./common/common.sh"
else
	. "../common/common.sh"
fi

showmsg "您的系统需要安装防火墙（IPFW）吗？[y/n]"	"Do you want to install a firewall(IPFW)?[y/n]"

if [ `uname` != 'FreeBSD' ]; then
	showmsg '这不是FreeBSD系统，不能安装IPFW'		'Sorry! This system is not FreeBSD, Can not to install IPFW!'
	exit
fi

read confirm
if [ "$confirm" = 'Y' ] || [ "$confirm" = 'y' ] || [ "$confirm" = '' ]; then
	if [ ! `grep -l "LuNamp ipfw start"    "/etc/rc.conf"` ]; then
		cat "${i_code_root}/firewall/rc.conf_add.conf" >> "/etc/rc.conf"
	fi
	cat "${i_code_root}/firewall/ipfw.conf" > "/etc/ipfw.conf"
	showmsg	"IPFW 安装成功，谢谢使用！建议您在LuNamp安装完成后，重启系统（reboot）。"		"IPFW is installed, We suggest you reboot your system(reboot) in the LuNamp installed."
else
	showmsg "您已经取消了安装防火墙（IPFW）！"    "You has canceled to install IPFW."
fi
