#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

. "/usr/local/LuNamp/cmd/common.inc"

mysql_user='lu_mysql'

datadir=${mysql_data_path}
if [ ! -d "${mysql_data_path}" ]; then
	mkdir -p $datadir
fi

chown -R ${mysql_user} $datadir
chmod -R 2770 $datadir

chown -R root:${mysql_user} ${mysql_root}/.

${mysql_root}/bin/mysql_install_db --user=root --basedir=${mysql_root}  --datadir=${mysql_data_path};
echo
chown -R root ${mysql_root}/.
chown -R ${mysql_user} ${mysql_root}/var

chown -R ${mysql_user}:${mysql_user} $datadir
chmod -R 2770 $datadir

${mysql_root}/bin/mysqld_safe --user=${mysql_user} --datadir=${mysql_data_path} &
echo

if [ `ps -Af | grep "mysqld_safe" | grep -vc 'grep'` != '0' ]; then
	showmsg    "${color_green}MySQL 修复成功！"    "MySQL was restored successfully.${color_white}"
else
	showmsg "${color_red}MySQL 修复失败！"    "MySQL was restored failed.${color_white}"
fi
