#!/bin/sh
#
# ******************************************************************************
# 感谢您使用LuNamp，本软件遵循GPL协议，详情请看：http://www.gnu.org/copyleft/gpl.html
# Thank you for choosing LuNamp, This program is free software; 
# you can redistribute it and/or modify it under the terms of the GNU General 
# Public License as published by the Free Software Foundation; 
# either version 2 of the License, or (at your option) any later version.
# http://www.gnu.org/copyleft/gpl.html
# ------------------------------------++++
# 软件名：	LuNamp
# 作者：		刘新（网名：爱洞特漏）
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# ------------------------------------++++
# Software:  LuNamp
# Author:    Liu Xin
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# ------------------------------------++++
# Thank you for choosing LuNamp!
# ******************************************************************************
#

if [ `id -u` != "0" ]; then
	echo "请用root用户安装LuNamp（请在命令行输入：su root）！"
	echo
	echo "Please use root user to install LuNamp (cmd: su root)!"
	echo
	exit;
fi

if [ -e "./code.tar.gz" ] && [ ! -d "./code" ]; then
	tar -zxvf code.tar.gz
fi
if [ -e "./soft.tar.gz" ] && [ ! -d "./soft" ]; then
	tar -zxvf soft.tar.gz
fi

. "./code/common/common.sh"

echo $color_yellow
echo
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "+"
echo "+ Software:  $LuNamp_ver"
echo "+ Author:    Liu Xin  [网名：爱洞特漏]"                                               
echo "+ Website:   www.zijidelu.org  [自己的路]"
echo "+ Email:     service@zijidelu.org"
echo "+ Thank you for choosing ${LuNamp_ver}!"
echo "+"
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

if [ "$1" != '-y' ]; then
	echo $color_green
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	echo "+"
	echo "+ [${color_cyan}1${color_green}] Install LuNamp(${color_yellow}All${color_green}--Default).(安装LuNamp所有组件)"
	echo "+ [2] ${color_red}Uninstall${color_green} LuNamp(删除LuNamp)."
	echo "+ [3] Install ${color_yellow}Install LuNamp, But without Apache${color_green}.(无Apache)"
	#echo "+ [4] Install ${color_yellow}Install LuNamp, But without Bind, Apache${color_green}.(无Bind)"
	echo "+ [0] Cancel.(取消)"
	echo "+"
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

	showmsg "请输入您的选择，然后回车！"   "Please choosing your option and press enter!  ${color_green}[1/2/3/4/0] ${color_white}"
	read install_num
fi

echo $color_white

if [ "$install_num" = '' ] || [ "$1" = '-y' ]; then
	install_num='1';
fi

if [ "$install_num" = '1' ]; then
	echo "allow_apache='Yes'" >> "${i_code_root}/common/common.sh";
	echo "allow_nginx='Yes'" >> "${i_code_root}/common/common.sh";
elif [ "$install_num" = '3' ]; then
	echo "allow_apache='No'" >> "${i_code_root}/common/common.sh";
	echo "allow_nginx='Yes'" >> "${i_code_root}/common/common.sh";
fi

cd ${i_code_root}
chmod -R 750 ./*
if [ "$install_num" != '0' ]; then
	if [ "$install_num" = '2' ]; then
		./uninstall.sh
	else
		if [ "$1" = '-y' ]; then
			./install.sh -y
		else
			./install.sh
		fi
	fi
fi


