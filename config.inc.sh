#!/bin/sh
#
# ******************************************************************************
# 官方网站：	www.zijidelu.org
# 服务邮箱：	service@zijidelu.org
# 作者：		刘新（网名：爱洞特漏）
# 电话：		+86-13688995218
# ------------------------------------++++
# Website:   www.zijidelu.org
# Email:     service@zijidelu.org
# Author:    Liu Xin
# Tel:       +86-13688995218
# ------------------------------------++++
# Thank you for choosing zijidelu's software!
# ******************************************************************************
#

#####################
# 设置好本文件后，不会再询问下载镜像点和LUM版本号，直接装便可

down='';	#下载镜像。填"down"为中国，填"down3"为美国

lum_ver='';	#LUM版本号

jump_env_soft='';	#是否跳过环境软件的安装
